package de.microtema.monitoring.utils;

import de.microtema.monitoring.commons.models.ProcessStatus;
import de.microtema.monitoring.definition.model.ProcessDefinition;
import de.microtema.monitoring.event.command.model.ReportEvent;
import de.microtema.monitoring.event.command.model.ReportStatus;
import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.StringUtils;
import org.camunda.bpm.model.bpmn.instance.FlowElement;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Optional;

import static de.microtema.monitoring.constants.Constants.MAX_TEXT_SIZE;

@UtilityClass
public class CommonUtils {

    public String substring(String text, int maxLength, boolean ellipsis) {

        if (StringUtils.length(text) > maxLength) {

            return text.substring(0, ellipsis ? maxLength - 3 : maxLength) + (ellipsis ? "..." : StringUtils.EMPTY);
        }

        return text;
    }

    public static String substringPart(String part, int startIndex) {

        if (StringUtils.isEmpty(part)) {
            return part;
        }

        var partLength = part.length();

        var beginIndex = Math.min(partLength, startIndex);
        var endIndex = Math.min(partLength, startIndex + MAX_TEXT_SIZE);

        return part.substring(beginIndex, endIndex);
    }

    public static ProcessStatus getReportStatus(String elementId, ProcessDefinition processDefinition, ReportStatus reportEventStatus) {

        var processStatus = ProcessStatus.valueOf(reportEventStatus.name());

        if (Objects.isNull(processDefinition)) {

            return processStatus;
        }

        var bpmnModelInstance = processDefinition.getBpmnModelInstance();

        if (Objects.isNull(bpmnModelInstance)) {
            return processStatus;
        }

        FlowElement flowElement = bpmnModelInstance.getModelElementById(elementId);

        if (Objects.isNull(flowElement)) {

            return processStatus;
        }

        var status = BpmnModelInstanceUtils.getExtensionPropertyValue(flowElement, "status");

        if (StringUtils.isEmpty(status)) {

            return processStatus;
        }

        return ProcessStatus.valueOf(status);
    }

    public static ProcessStatus getReportStatus(ReportEvent reportEvent, ProcessDefinition processDefinition) {

        var reportEventStatus = ProcessStatus.valueOf(reportEvent.getStatus().name());

        if (Objects.isNull(processDefinition)) {

            return reportEventStatus;
        }

        var bpmnModelInstance = processDefinition.getBpmnModelInstance();

        if (Objects.isNull(bpmnModelInstance)) {
            return reportEventStatus;
        }

        FlowElement flowElement = bpmnModelInstance.getModelElementById(reportEvent.getElementId());

        if (Objects.isNull(flowElement)) {

            return reportEventStatus;
        }

        var status = BpmnModelInstanceUtils.getExtensionPropertyValue(flowElement, "status");

        if (StringUtils.isEmpty(status)) {

            return reportEventStatus;
        }

        return ProcessStatus.valueOf(status);
    }

    public static String getReportName(ReportEvent reportEvent, ProcessDefinition processDefinition) {

        return getReportName(reportEvent.getElementId(), processDefinition);
    }

    public static String getReportName(String elementId, ProcessDefinition processDefinition) {

        if (Objects.isNull(processDefinition)) {
            return elementId;
        }

        var bpmnModelInstance = processDefinition.getBpmnModelInstance();

        if (Objects.isNull(bpmnModelInstance)) {
            return elementId;
        }

        FlowElement flowElement = bpmnModelInstance.getModelElementById(elementId);

        return Optional.ofNullable(flowElement).map(FlowElement::getName).orElse(elementId);
    }

    public static LocalDateTime min(LocalDateTime localDateTime, LocalDateTime other) {

        if (Objects.isNull(localDateTime)) {
            return other;
        }

        if (Objects.isNull(other)) {
            return localDateTime;
        }

        if (localDateTime.isBefore(other)) {
            return localDateTime;
        }

        return other;
    }

    public static LocalDateTime max(LocalDateTime localDateTime, LocalDateTime other) {

        if (Objects.isNull(localDateTime)) {
            return other;
        }

        if (Objects.isNull(other)) {
            return localDateTime;
        }

        if (localDateTime.isAfter(other)) {
            return localDateTime;
        }

        return other;
    }

    public static ProcessStatus max(ProcessStatus reportStatus, ProcessStatus other) {

        if (Objects.isNull(reportStatus)) {
            return other;
        }

        if (Objects.isNull(other)) {
            return reportStatus;
        }

        if (reportStatus.compareTo(other) > 0) {
            return reportStatus;
        }

        return other;
    }

    public static String max(String str, String other) {

        return Optional.ofNullable(StringUtils.trimToNull(str)).orElse(other);
    }

    public static Long parseLong(String str, Long defaultValue) {

        if (StringUtils.isEmpty(str)) {
            return defaultValue;
        }

        return Long.parseLong(str);
    }
}
