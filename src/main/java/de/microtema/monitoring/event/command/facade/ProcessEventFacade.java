package de.microtema.monitoring.event.command.facade;

import de.microtema.monitoring.event.command.model.ReportEvent;
import de.microtema.monitoring.event.command.service.ProcessEventWriteService;
import de.microtema.monitoring.report.model.ProcessReport;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ProcessEventFacade {

    private final ProcessEventWriteService processEventWriteService;

    public ProcessReport saveReportEvent(ReportEvent event) {

        var uuid = processEventWriteService.saveProcessReport(event);

        var processReport = new ProcessReport();

        processReport.setId(uuid);

        return processReport;
    }
}
