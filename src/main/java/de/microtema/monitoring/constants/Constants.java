package de.microtema.monitoring.constants;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Constants {

    public static final int MAX_TEXT_SIZE = 2000;

    public static final int MIN_RETENTION_DAYS = 7;
}
