package de.microtema.monitoring.definition.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;

import java.time.LocalDateTime;

public class ProcessDefinition {

    private String id;

    private String definitionKey;

    private String boundedContext;

    private String name;

    private String displayName;

    private Integer definitionVersion;

    private Integer majorVersion;

    private String description;

    private String diagram;

    private LocalDateTime deployTime;

    private ProcessDefinitionStatus definitionStatus;

    private LocalDateTime lastHeartBeat;

    private int retentionTime;

    @JsonIgnore
    private BpmnModelInstance bpmnModelInstance;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDefinitionKey() {
        return definitionKey;
    }

    public void setDefinitionKey(String definitionKey) {
        this.definitionKey = definitionKey;
    }

    public String getBoundedContext() {
        return boundedContext;
    }

    public void setBoundedContext(String boundedContext) {
        this.boundedContext = boundedContext;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Integer getDefinitionVersion() {
        return definitionVersion;
    }

    public void setDefinitionVersion(Integer definitionVersion) {
        this.definitionVersion = definitionVersion;
    }

    public Integer getMajorVersion() {
        return majorVersion;
    }

    public void setMajorVersion(Integer majorVersion) {
        this.majorVersion = majorVersion;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDiagram() {
        return diagram;
    }

    public void setDiagram(String diagram) {
        this.diagram = diagram;
    }

    public LocalDateTime getDeployTime() {
        return deployTime;
    }

    public void setDeployTime(LocalDateTime deployTime) {
        this.deployTime = deployTime;
    }

    public ProcessDefinitionStatus getDefinitionStatus() {
        return definitionStatus;
    }

    public void setDefinitionStatus(ProcessDefinitionStatus definitionStatus) {
        this.definitionStatus = definitionStatus;
    }

    public LocalDateTime getLastHeartBeat() {
        return lastHeartBeat;
    }

    public void setLastHeartBeat(LocalDateTime lastHeartBeat) {
        this.lastHeartBeat = lastHeartBeat;
    }

    public BpmnModelInstance getBpmnModelInstance() {
        return bpmnModelInstance;
    }

    public void setBpmnModelInstance(BpmnModelInstance bpmnModelInstance) {
        this.bpmnModelInstance = bpmnModelInstance;
    }

    public int getRetentionTime() {
        return retentionTime;
    }

    public void setRetentionTime(int retentionTime) {
        this.retentionTime = retentionTime;
    }
}
