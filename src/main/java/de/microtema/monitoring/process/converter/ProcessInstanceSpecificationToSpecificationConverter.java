package de.microtema.monitoring.process.converter;

import de.microtema.model.converter.Converter;
import de.microtema.monitoring.commons.models.ProcessStatus;
import de.microtema.monitoring.process.model.ProcessInstanceSpecification;
import de.microtema.monitoring.process.repository.ProcessInstanceEntity;
import de.microtema.monitoring.timerange.model.TimeRangeType;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.Path;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Optional;

@Component
public class ProcessInstanceSpecificationToSpecificationConverter implements Converter<Specification<ProcessInstanceEntity>, ProcessInstanceSpecification> {

    @Override
    public Specification<ProcessInstanceEntity> convert(ProcessInstanceSpecification orig) {

        return (root, criteriaQuery, criteriaBuilder) -> {

            var predicate = getEqualsPredicate(root, criteriaBuilder, "definitionKey", orig.getDefinitionKey());
            var processStatusPredicate = getStatusPredicate(root, criteriaBuilder, "instanceStatus", orig.getProcessStatus());
            var timeRangePredicate = getTimeRangePredicate(root, criteriaBuilder, orig.getTimeRange(), orig.getStartDate(), orig.getEndDate());

            predicate = criteriaBuilder.and(predicate, processStatusPredicate);

            if (Objects.nonNull(timeRangePredicate)) {

                predicate = criteriaBuilder.and(predicate, timeRangePredicate);
            }

            var orPredicate = getOrPredicate(orig, root, criteriaBuilder);

            if (Objects.nonNull(orPredicate)) {

                predicate = criteriaBuilder.and(predicate, orPredicate);
            }

            return criteriaBuilder.and(predicate);
        };
    }

    private Predicate getTimeRangePredicate(Root<ProcessInstanceEntity> root, CriteriaBuilder criteriaBuilder, TimeRangeType timeRange, LocalDateTime startDate, LocalDateTime endDate) {

        var startDateTime = timeRange.getStartDateTime(startDate);
        var endDateTime = timeRange.getEndDateTime(endDate);

        return criteriaBuilder.between(root.get("instanceStartTime"), startDateTime, endDateTime);
    }

    private Predicate getOrPredicate(ProcessInstanceSpecification orig, Root<ProcessInstanceEntity> root, CriteriaBuilder criteriaBuilder) {

        var predicates = new ArrayList<Predicate>();

        var predicate = getLikePredicate(root, criteriaBuilder, "instanceName", StringUtils.trimToEmpty(orig.getQuery()));
        CollectionUtils.addIgnoreNull(predicates, predicate);

        predicate = getLikePredicate(root, criteriaBuilder, "instanceDescription", orig.getQuery());
        CollectionUtils.addIgnoreNull(predicates, predicate);

        predicate = getLikePredicate(root, criteriaBuilder, "processBusinessKey", orig.getQuery());
        CollectionUtils.addIgnoreNull(predicates, predicate);

        predicate = getLikePredicate(root, criteriaBuilder, "starterId", orig.getQuery());
        CollectionUtils.addIgnoreNull(predicates, predicate);

        predicate = getLikePredicate(root, criteriaBuilder, "referenceId", orig.getQuery());
        CollectionUtils.addIgnoreNull(predicates, predicate);

        predicate = getLikePredicate(root, criteriaBuilder, "referenceType", orig.getQuery());
        CollectionUtils.addIgnoreNull(predicates, predicate);

        predicate = getLikePredicate(root, criteriaBuilder, "referenceValue", orig.getQuery());
        CollectionUtils.addIgnoreNull(predicates, predicate);

        if (CollectionUtils.isEmpty(predicates)) {
            return null;
        }

        return criteriaBuilder.or(predicates.toArray(new Predicate[0]));
    }

    private Predicate getEqualsPredicate(Root<ProcessInstanceEntity> root, CriteriaBuilder criteriaBuilder, String property, String query) {

        if (StringUtils.isEmpty(query)) {
            return criteriaBuilder.and();
        }

        Path<String> path = root.get(property);

        return criteriaBuilder.equal(criteriaBuilder.lower(path), query.toLowerCase());
    }

    private Predicate getStatusPredicate(Root<ProcessInstanceEntity> root, CriteriaBuilder criteriaBuilder, String property, ProcessStatus processStatus) {

        if (ProcessStatus.ALL == processStatus || Objects.isNull(processStatus)) {
            return criteriaBuilder.and();
        }

        Path<ProcessStatus> path = root.get(property);

        return criteriaBuilder.equal(path, processStatus);
    }

    private Predicate getLikePredicate(Root<ProcessInstanceEntity> root, CriteriaBuilder criteriaBuilder, String property, String query) {

        if (Objects.isNull(query)) {
            return null;
        }

        Path<String> path = root.get(property);

        return criteriaBuilder.like(criteriaBuilder.lower(path), "%" + query.toLowerCase() + "%");
    }
}
