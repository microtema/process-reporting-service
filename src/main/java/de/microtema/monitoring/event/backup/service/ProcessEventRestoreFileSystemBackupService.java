package de.microtema.monitoring.event.backup.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.microtema.monitoring.commons.backup.model.BackupType;
import de.microtema.monitoring.commons.backup.service.ProcessRestoreFileSystemBackupService;
import de.microtema.monitoring.event.command.repository.ProcessEventEntity;
import de.microtema.monitoring.event.command.repository.ProcessEventRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProcessEventRestoreFileSystemBackupService extends ProcessRestoreFileSystemBackupService<ProcessEventEntity> {

    private static final TypeReference<List<ProcessEventEntity>> TYPE_REFERENCE = new TypeReference<>() {
    };

    public ProcessEventRestoreFileSystemBackupService(ObjectMapper objectMapper, ProcessEventRepository repository,
                                                      ProcessEventReadFileSystemBackupService readFileSystemBackupService) {
        super(objectMapper, repository, readFileSystemBackupService);
    }

    @Override
    public TypeReference<List<ProcessEventEntity>> getTypeReference() {
        return TYPE_REFERENCE;
    }

    @Override
    public BackupType getBackupType() {
        return BackupType.EVENT;
    }
}
