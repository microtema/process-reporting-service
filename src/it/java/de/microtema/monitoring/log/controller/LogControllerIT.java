package de.microtema.monitoring.log.controller;

import de.microtema.model.builder.annotation.Model;
import de.microtema.model.builder.enums.ModelType;
import de.microtema.model.builder.util.CollectionUtil;
import de.microtema.model.builder.util.FieldInjectionUtil;
import de.microtema.monitoring.Application;
import de.microtema.monitoring.log.model.*;
import de.microtema.monitoring.log.repository.LogRepository;
import jakarta.inject.Inject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {Application.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class LogControllerIT {

    @Inject
    LogController sut;

    @Inject
    LogRepository repository;

    @Inject
    TestRestTemplate restTemplate;

    @Model(type = ModelType.MAX)
    LogReportEvent reportEvent;

    @Model
    LogLevel logLevel;

    String url = "/rest/api/log";

    @BeforeEach
    void setUp() {

        FieldInjectionUtil.injectFields(this);

        reportEvent.setLevel(logLevel.name());

        sut.saveLogEvent(reportEvent);
    }

    @Test
    void saveLogEvent() {

        var response = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(reportEvent), String.class);

        assertNotNull(response);
        assertSame(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void getLogDataPage() {

        url += "?page=0&size=10&query=&properties=!logTimestamp";

        var response = restTemplate.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<String>() {
        });

        assertNotNull(response);
        assertSame(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    void getLogData() {

        var entities = repository.findAll();

        var logEntity = CollectionUtil.random(entities);

        var uuid = logEntity.getUuid();

        url += "/" + uuid;

        var response = restTemplate.exchange(url, HttpMethod.GET, null, LogData.class);

        assertNotNull(response);
        assertSame(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(uuid, response.getBody().getId());
    }

    @Test
    void getLogLevelList() {

        url += "/level";

        var response = restTemplate.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<List<LogLevelDTO>>() {
        });

        assertNotNull(response);
        assertSame(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertFalse(response.getBody().isEmpty());
    }

    @Test
    void getRunTimes() {

        url += "/runtime";

        var response = restTemplate.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<List<RuntimeData>>() {
        });

        assertNotNull(response);
        assertSame(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertFalse(response.getBody().isEmpty());
    }
}
