package de.microtema.monitoring.event.query.service;

import de.microtema.monitoring.commons.converter.PageRequestDTOToPageRequestConverter;
import de.microtema.monitoring.commons.models.PageRequestDTO;
import de.microtema.monitoring.commons.models.ProcessStatus;
import de.microtema.monitoring.event.query.converter.ProcessEventEntityToProcessReportConverter;
import de.microtema.monitoring.event.query.converter.ProcessEventSpecificationToSpecificationConverter;
import de.microtema.monitoring.event.command.repository.ProcessEventRepository;
import de.microtema.monitoring.report.model.ProcessReport;
import de.microtema.monitoring.report.model.ProcessReportSpecification;
import de.microtema.monitoring.timerange.model.TimeRangeType;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ProcessEventReadService {

    private final ProcessEventRepository repository;
    private final PageRequestDTOToPageRequestConverter pageRequestConverter;
    private final ProcessEventEntityToProcessReportConverter processReportConverter;
    private final ProcessEventSpecificationToSpecificationConverter specificationConverter;

    public Page<ProcessReport> getProcessReports(int page, int size, String properties, String query, String definitionKey, ProcessStatus reportStatus, TimeRangeType timeRange, LocalDateTime startDate, LocalDateTime endDate) {

        var processReportSpecification = ProcessReportSpecification.builder()
                .query(query)
                .definitionKey(definitionKey)
                .reportStatus(reportStatus)
                .timeRange(timeRange)
                .startDate(startDate)
                .endDate(endDate)
                .build();

        var pageRequestDTO = PageRequestDTO.of(page, size, properties);

        var specification = specificationConverter.convert(processReportSpecification);
        var pageable = pageRequestConverter.convert(pageRequestDTO);

        var entityPage = repository.findAll(specification, pageable);

        return entityPage.map(processReportConverter::convert);
    }

    public List<ProcessReport> getProcessEvents(String definitionKey, String processBusinessKey, int retryCount) {

        var reports = repository.findAllBy(definitionKey, processBusinessKey, retryCount);

        return processReportConverter.convertList(reports);
    }

    public ProcessReport getProcessReport(String uuid) {

        return repository.findById(uuid).map(processReportConverter::convert).orElse(null);
    }
}
