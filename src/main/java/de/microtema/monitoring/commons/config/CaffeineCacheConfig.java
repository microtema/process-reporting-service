package de.microtema.monitoring.commons.config;

import com.github.benmanes.caffeine.cache.Caffeine;
import org.springframework.cache.CacheManager;
import org.springframework.cache.caffeine.CaffeineCache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Collections;
import java.util.concurrent.TimeUnit;

@Configuration
public class CaffeineCacheConfig {

    @Bean
    public CacheManager cacheManager() {

        var definitionCache = new CaffeineCache("definition", Caffeine.newBuilder().expireAfterAccess(1, TimeUnit.HOURS).build());

        var manager = new SimpleCacheManager();

        manager.setCaches(Collections.singletonList(definitionCache));

        return manager;
    }
}
