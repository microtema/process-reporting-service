package de.microtema.monitoring.timerange.service;

import de.microtema.monitoring.timerange.converter.TimeRangeTypeToTimeRangeEntryConverter;
import de.microtema.monitoring.timerange.model.TimeRangeEntry;
import de.microtema.monitoring.timerange.model.TimeRangeType;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class TimeRangeService {

    private final TimeRangeTypeToTimeRangeEntryConverter timeRangeTypeToTimeRangeEntryConverter;

    public List<TimeRangeEntry> getTimeRangeList() {

        return timeRangeTypeToTimeRangeEntryConverter.convertList(TimeRangeType.values());
    }
}
