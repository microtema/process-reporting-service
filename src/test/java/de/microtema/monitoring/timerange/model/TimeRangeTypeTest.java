package de.microtema.monitoring.timerange.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TimeRangeTypeTest {

    TimeRangeType sut;

    @Test
    void size() {

        assertEquals(8, TimeRangeType.values().length);
    }
}
