package de.microtema.monitoring.event.query.facade;

import de.microtema.monitoring.commons.models.ProcessStatus;
import de.microtema.monitoring.commons.models.ProcessStatusDTO;
import de.microtema.monitoring.commons.service.ProcessStatusService;
import de.microtema.monitoring.event.query.service.ProcessEventReadService;
import de.microtema.monitoring.report.model.ProcessReport;
import de.microtema.monitoring.timerange.model.TimeRangeType;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Component
@RequiredArgsConstructor
public class ProcessEventViewFacade {

    private final ProcessStatusService processStatusService;
    private final ProcessEventReadService processReportReadService;

    public Page<ProcessReport> getProcessReports(int page, int size, String properties, String query, String definitionKey, ProcessStatus reportStatus, TimeRangeType timeRange, LocalDateTime startDate, LocalDateTime endDate) {

        return processReportReadService.getProcessReports(page, size, properties, query, definitionKey, reportStatus, timeRange, startDate, endDate);
    }

    public List<ProcessReport> getProcessEvents(String definitionKey, String processBusinessKey, int retryCount) {

        return processReportReadService.getProcessEvents(definitionKey, processBusinessKey, retryCount);
    }

    public ProcessReport getProcessReport(String uuid) {

        return processReportReadService.getProcessReport(uuid);
    }

    public List<ProcessStatusDTO> getReportStatusList() {

        return processStatusService.getProcessStatusList();
    }
}
