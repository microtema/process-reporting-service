package de.microtema.monitoring.process.service;

import de.microtema.monitoring.commons.converter.PageRequestDTOToPageRequestConverter;
import de.microtema.monitoring.commons.models.PageRequestDTO;
import de.microtema.monitoring.commons.models.ProcessStatus;
import de.microtema.monitoring.process.converter.ProcessInstanceSpecificationToSpecificationConverter;
import de.microtema.monitoring.process.converter.ProcessInstanceEntityToProcessInstanceConverter;
import de.microtema.monitoring.process.model.ProcessInstance;
import de.microtema.monitoring.process.model.ProcessInstanceSpecification;
import de.microtema.monitoring.process.repository.ProcessInstanceRepository;
import de.microtema.monitoring.timerange.model.TimeRangeType;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Service
@Transactional
@RequiredArgsConstructor
public class ProcessInstanceReadService {

    private final ProcessInstanceRepository repository;
    private final PageRequestDTOToPageRequestConverter pageRequestConverter;
    private final ProcessInstanceEntityToProcessInstanceConverter processInstanceConverter;
    private final ProcessInstanceSpecificationToSpecificationConverter specificationConverter;

    public Page<ProcessInstance> getProcesses(int page, int size, String properties, String query, String definitionKey, ProcessStatus processStatus, TimeRangeType timeRange, LocalDateTime startDate, LocalDateTime endDate) {

        if (processStatus == ProcessStatus.COMPLETED) {
            processStatus = ProcessStatus.PROCESS_COMPLETED;
        }

        var pageRequestDTO = PageRequestDTO.of(page, size, properties);
        var pageable = pageRequestConverter.convert(pageRequestDTO);
        var processInstanceSpecification = ProcessInstanceSpecification.builder()
                .definitionKey(definitionKey)
                .processStatus(processStatus)
                .timeRange(timeRange)
                .startDate(startDate)
                .endDate(endDate)
                .query(query).build();

        var specification = specificationConverter.convert(processInstanceSpecification);

        return repository.findAll(specification, pageable).map(processInstanceConverter::convert);
    }

    public ProcessInstance findProcessInstance(String businessKey, int retryCount) {

        return repository.findByProcessBusinessKeyAndRetryCount(businessKey, retryCount).map(processInstanceConverter::convert).orElse(null);
    }

}
