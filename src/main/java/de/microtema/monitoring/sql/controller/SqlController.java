package de.microtema.monitoring.sql.controller;

import de.microtema.monitoring.sql.facade.SqlFacade;
import de.microtema.monitoring.sql.model.SqlRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/rest/api/sql")
public class SqlController {

    private final SqlFacade facade;

    @PostMapping
    public ResponseEntity<Object> execute(@RequestBody SqlRequest sql) {

        return ResponseEntity.ok(facade.execute(sql));
    }
}
