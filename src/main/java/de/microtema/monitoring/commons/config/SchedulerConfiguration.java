package de.microtema.monitoring.commons.config;

import net.javacrumbs.shedlock.core.LockProvider;
import net.javacrumbs.shedlock.provider.jdbctemplate.JdbcTemplateLockProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SimpleAsyncTaskExecutor;

import javax.sql.DataSource;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@Configuration
public class SchedulerConfiguration {

    @Bean
    public LockProvider lockProvider(DataSource dataSource) {

        return new JdbcTemplateLockProvider(dataSource);
    }

    @Bean(destroyMethod = "shutdown")
    public ScheduledExecutorService primaryExecutorService() {

        return Executors.newSingleThreadScheduledExecutor();
    }

    @Bean
    public Executor getAsyncExecutor() {

        return new SimpleAsyncTaskExecutor();
    }
}
