package de.microtema.monitoring.smb.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "file-system")
public class FileSystemProperties {

    /**
     * Server Url host
     */
    private String server;

    /**
     * directory path
     */
    private String path;

    private String username;

    private String password;

    private String domain;
}
