package de.microtema.monitoring.log.service;

import de.microtema.monitoring.constants.Constants;
import de.microtema.monitoring.definition.service.ProcessDefinitionService;
import de.microtema.monitoring.log.repository.LogRepository;
import de.microtema.monitoring.utils.BpmnModelInstanceUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import net.javacrumbs.shedlock.spring.annotation.SchedulerLock;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Log4j2
@Service
@RequiredArgsConstructor
public class LogCleanupService {

    private final LogRepository repository;
    private final LogReadService logReadService;
    private final ProcessDefinitionService processDefinitionService;

    @Scheduled(cron = "0 1 1 * * ?")
    @SchedulerLock(name = "LogCleanupService_cleanup", lockAtLeastFor = "PT5M", lockAtMostFor = "PT14M")
    public void cleanup() {

        var runtimes = logReadService.getRunTimes();

        for (var runtime : runtimes) {

            var definitionKey = runtime.getDefinitionKey();
            var displayName = runtime.getDefinitionKey();
            var definition = processDefinitionService.getProcessDefinitionByDefinitionKey(definitionKey);
            var retentionDays = Constants.MIN_RETENTION_DAYS;

            if (Objects.nonNull(definition)) {

                retentionDays = BpmnModelInstanceUtils.getHistoryTimeToLive(definition.getBpmnModelInstance(), retentionDays);
                displayName = definition.getDisplayName();
            }

            var count = repository.cleanup(definitionKey, retentionDays);

            log.info("{} {} Logs(s) older then {} day(s) removed", count, displayName, retentionDays);
        }
    }
}
