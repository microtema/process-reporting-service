package de.microtema.monitoring.event.command.service;

import de.microtema.monitoring.definition.model.ProcessDefinition;
import de.microtema.monitoring.definition.service.ProcessDefinitionService;
import de.microtema.monitoring.event.command.converter.ReportEventToProcessEventEntityConverter;
import de.microtema.monitoring.event.command.model.ReportEvent;
import de.microtema.monitoring.event.command.repository.ProcessEventEntity;
import de.microtema.monitoring.event.command.repository.ProcessEventRepository;
import de.microtema.monitoring.utils.BpmnModelInstanceUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Log4j2
@Service
@Transactional
@RequiredArgsConstructor
public class ProcessEventWriteService {

    private final ProcessEventRepository repository;
    private final ProcessDefinitionService processDefinitionService;
    private final ReportEventToProcessEventEntityConverter reportEventToProcessEventEntityConverter;

    public String saveProcessReport(ReportEvent event) {

        var definition = getProcessDefinition(event);

        ProcessEventEntity processEventEntity = reportEventToProcessEventEntityConverter.convert(event, definition);

        return repository.save(processEventEntity).getUuid();
    }

    private ProcessDefinition getProcessDefinition(ReportEvent reportEvent) {

        var processId = reportEvent.getProcessId();
        var processVersion = Optional.ofNullable(reportEvent.getProcessVersion()).orElse("1.0");
        var majorVersion = BpmnModelInstanceUtils.getMajorVersion(processVersion);

        return processDefinitionService.getProcessDefinitionByDefinitionKeyAndMajorVersion(processId, majorVersion);
    }
}
