package de.microtema.monitoring.commons.config;

import com.fasterxml.jackson.databind.module.SimpleModule;
import de.microtema.monitoring.commons.serde.OffsetDateTimeDeserializer;
import de.microtema.monitoring.commons.serde.OffsetDateTimeJsonSerializer;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;

import java.time.Duration;
import java.time.OffsetDateTime;
import java.util.concurrent.TimeUnit;

@Configuration
public class ApplicationConfig {

    private static final long CONNECTION_TIMEOUT = TimeUnit.MINUTES.toMillis(1);

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {

        var restTemplate = builder.setConnectTimeout(Duration.ofMillis(CONNECTION_TIMEOUT)).build();

        restTemplate.setUriTemplateHandler(defaultUriBuilderFactory());

        return restTemplate;
    }

    public DefaultUriBuilderFactory defaultUriBuilderFactory() {

        var defaultUriBuilderFactory = new DefaultUriBuilderFactory();

        defaultUriBuilderFactory.setEncodingMode(DefaultUriBuilderFactory.EncodingMode.NONE);

        return defaultUriBuilderFactory;
    }

    @Bean
    public SimpleModule simpleModule() {

        var simpleModule = new SimpleModule();

        simpleModule.addDeserializer(OffsetDateTime.class, new OffsetDateTimeDeserializer());

        simpleModule.addSerializer(OffsetDateTime.class, new OffsetDateTimeJsonSerializer());

        return simpleModule;
    }

}
