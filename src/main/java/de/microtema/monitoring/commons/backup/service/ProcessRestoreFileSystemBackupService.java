package de.microtema.monitoring.commons.backup.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.microtema.monitoring.commons.backup.model.BackupType;
import de.microtema.monitoring.commons.repository.BaseEntity;
import de.microtema.monitoring.commons.repository.BaseEntityRepository;
import de.microtema.monitoring.timerange.model.TimeRangeType;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;

import java.io.File;
import java.io.FileInputStream;
import java.time.LocalDateTime;
import java.util.List;

@Log4j2
@RequiredArgsConstructor
public abstract class ProcessRestoreFileSystemBackupService<T extends BaseEntity> {

    private final ObjectMapper objectMapper;
    private final BaseEntityRepository<T> repository;
    private final ProcessReadFileSystemBackupService systemBackupService;

    @SneakyThrows
    public synchronized void restore(int page, int size, String query, String definitionKey, TimeRangeType timeRange, LocalDateTime startDate, LocalDateTime endDate) {

        var backups = systemBackupService.getBackups(page, size, query, definitionKey, timeRange, startDate, endDate);

        if (backups.isEmpty()) {

            return;
        }

        var content = backups.getContent();

        content.forEach(it -> executeBackup(it.getPath()));

        restore(page + 1, size, query, definitionKey, timeRange, startDate, endDate);
    }

    public abstract TypeReference<List<T>> getTypeReference();

    public abstract BackupType getBackupType();

    @SneakyThrows
    private void executeBackup(String fileName) {

        var file = new File(fileName);

        var fileInputStream = new FileInputStream(file);

        var entries = objectMapper.readValue(fileInputStream, getTypeReference());

        entries.forEach(it -> it.setUuid(null));

        repository.saveAllAndFlush(entries);

        var delete = file.delete();

        log.info("Backup {} successfully restored in database and deleted from {} FileSystem -> {}", fileName, getBackupType(), delete);
    }
}
