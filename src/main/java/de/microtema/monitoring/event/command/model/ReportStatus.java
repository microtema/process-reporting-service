package de.microtema.monitoring.event.command.model;

public enum ReportStatus {
    /**
     * Process/Activity has been queued
     */
    QUEUED,
    /**
     * Process/Activity has been started
     */
    STARTED,
    /**
     * Process/Activity has been restarted
     */
    RESTARTED,
    /**
     * Process/Activity has been completed
     */
    COMPLETED,
    /**
     * Process/Activity has been terminated and should be deleted
     */
    TERMINATED,

    PROCESS_COMPLETED,
    /**
     * Process/Activity has warning, but will not stop the process
     */
    WARNING,
    /**
     * Process/Activity has error and the process will be terminated
     */
    ERROR
}
