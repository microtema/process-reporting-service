package de.microtema.monitoring.definition.model;

import lombok.Data;

@Data
public class ProcessDefinitionStatus {

    private long queuedCount;

    private long progressCount;

    private long restartedCount;

    private long completedCount;

    private long terminatedCount;

    private long warningCount;

    private long failedCount;
}
