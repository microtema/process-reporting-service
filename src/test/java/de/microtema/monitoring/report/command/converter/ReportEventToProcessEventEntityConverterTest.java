package de.microtema.monitoring.report.command.converter;

import de.microtema.model.builder.annotation.Model;
import de.microtema.model.builder.enums.ModelType;
import de.microtema.model.builder.util.FieldInjectionUtil;
import de.microtema.monitoring.definition.model.ProcessDefinition;
import de.microtema.monitoring.event.command.converter.ReportEventToProcessEventEntityConverter;
import de.microtema.monitoring.event.command.model.ReportEvent;
import de.microtema.monitoring.event.command.model.ReportStatus;
import jakarta.inject.Inject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class ReportEventToProcessEventEntityConverterTest {

    @Inject
    ReportEventToProcessEventEntityConverter sut;

    @Model(type = ModelType.MAX)
    ReportEvent model;

    ProcessDefinition meta;

    @BeforeEach
    void setUp() {
        FieldInjectionUtil.injectFields(this);
    }

    @Test
    void convert() {

        model.setStatus(ReportStatus.STARTED);

        var answer = sut.convert(model, meta);

        assertNotNull(answer);

        assertEquals(model.getTransactionId(), answer.getProcessBusinessKey());

        assertEquals(model.getElementId(), answer.getReportId());
        assertEquals(model.getProcessId(), answer.getDefinitionKey());
        assertEquals(model.getMultipleInstanceIndex(), answer.getMultipleInstanceIndex());

        assertEquals(model.getElementId(), answer.getReportName());
        assertEquals(model.getStatus().name(), answer.getReportStatus().name());

        assertEquals(model.getErrorMessage(), answer.getErrorMessage());

        assertEquals(model.getEventTime(), answer.getReportStartTime());
        assertEquals(model.getPayload(), answer.getInputPayload());
        assertEquals(model.getRetryCount(), answer.getRetryCount());

        assertNotNull(answer.getReportStartTime());
        assertNotNull(answer.getReportEndTime());
    }
}
