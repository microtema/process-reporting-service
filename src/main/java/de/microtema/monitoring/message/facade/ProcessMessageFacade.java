package de.microtema.monitoring.message.facade;

import de.microtema.monitoring.commons.models.ProcessStatus;
import de.microtema.monitoring.commons.models.ProcessStatusDTO;
import de.microtema.monitoring.message.model.ProcessMessage;
import de.microtema.monitoring.message.service.ProcessMessageReadService;
import de.microtema.monitoring.timerange.model.TimeRangeType;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Component
@RequiredArgsConstructor
public class ProcessMessageFacade {

    private final ProcessMessageReadService processInstanceReadService;

    public Page<ProcessMessage> getProcessMessages(int page, int size, String properties, String query, String definitionKey, ProcessStatus processStatus, TimeRangeType timeRange, LocalDateTime startDate, LocalDateTime endDate) {

        return processInstanceReadService.getProcessMessages(page, size, properties, query, definitionKey, processStatus, timeRange, startDate, endDate);
    }

    public List<ProcessStatusDTO> getProcessMessageStatusList() {

        return processInstanceReadService.getProcessMessageStatusList();
    }
}
