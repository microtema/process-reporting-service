#!/bin/sh

mvn clean
mvn package -P prod

docker build -t microtema/process-reporting-service:7.1.3 .
docker push microtema/process-reporting-service:7.1.3
