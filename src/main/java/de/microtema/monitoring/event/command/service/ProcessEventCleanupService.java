package de.microtema.monitoring.event.command.service;

import de.microtema.monitoring.definition.service.ProcessDefinitionService;
import de.microtema.monitoring.event.backup.service.ProcessEventWriteFileSystemBackupService;
import de.microtema.monitoring.event.command.repository.ProcessEventRepository;
import de.microtema.monitoring.process.repository.ProcessInstanceRepository;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import net.javacrumbs.shedlock.spring.annotation.SchedulerLock;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Log4j2
@Service
@RequiredArgsConstructor
public class ProcessEventCleanupService {

    private final ProcessEventRepository processEventRepository;
    private final ProcessDefinitionService processDefinitionService;
    private final ProcessInstanceRepository processInstanceRepository;
    private final ProcessEventWriteFileSystemBackupService writeFileSystemBackupService;

    /**
     * Schedule every hour
     */
    @Scheduled(cron = "0 0 0/1 1/1 * ?")
    @SchedulerLock(name = "ProcessEventCleanupService_cleanup", lockAtLeastFor = "PT5M", lockAtMostFor = "PT14M")
    public void cleanup() {

        var definitions = processDefinitionService.getProcessDefinitions();

        for (var definition : definitions) {

            var definitionKey = definition.getDefinitionKey();
            var retentionDays = definition.getRetentionTime();
            var displayName = definition.getDisplayName();

            deleteTerminatedReports(definitionKey, displayName);

            var startTime = LocalDateTime.now().minusDays(retentionDays);

            writeFileSystemBackupService.backup(definitionKey, startTime);
        }
    }

    public void deleteTerminatedReports(String definitionKey, String displayName) {

        var page = processInstanceRepository.findAllTerminated(definitionKey);

        if (page.isEmpty()) {
            return;
        }

        var terminatedInstances = page.getContent();
        var totalElements = page.getTotalElements();

        var deletedCount = 0;

        for (var terminatedInstance : terminatedInstances) {

            var businessKey = terminatedInstance.getProcessBusinessKey();
            var retryCount = terminatedInstance.getRetryCount();

            var deleted = processEventRepository.deleteAllByDefinitionKeyAndProcessBusinessKeyAndRetryCount(definitionKey, businessKey, retryCount);

            deletedCount += deleted;
        }

        processEventRepository.flush();

        log.info("[{}] ({}) terminated instance(s) from [{}] successfully deleted.", totalElements, deletedCount, displayName);

        deleteTerminatedReports(definitionKey, displayName);
    }
}
