package de.microtema.monitoring.message.repository;

import de.microtema.monitoring.commons.models.ProcessStatus;
import de.microtema.monitoring.commons.repository.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.Immutable;

import jakarta.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Immutable
@EqualsAndHashCode(callSuper = true)
@Table(name = "PROCESS_INSTANCE_MESSAGE_VIEW")
public class ProcessMessageEntity extends BaseEntity {

    @Column
    private String definitionKey;

    @Column
    private String instanceName;

    @Column
    private String instanceDescription;

    @Column(columnDefinition = "Text")
    private String errorMessage;

    @Column
    private String errorMessageHash;

    @Column
    @Enumerated(value = EnumType.ORDINAL)
    private ProcessStatus instanceStatus;

    @Column
    private LocalDateTime instanceStartTime;

    @Column
    private LocalDateTime instanceEndTime;

    @Column
    private int totalCount;
}
