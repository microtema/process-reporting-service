package de.microtema.monitoring.csv.converter;

import com.opencsv.bean.StatefulBeanToCsvBuilder;
import de.microtema.model.converter.util.ClassUtil;
import lombok.SneakyThrows;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;

import java.io.OutputStreamWriter;
import java.util.List;
import java.util.Objects;

public abstract class AbstractCsvPageMessageConverter<P extends Page<T>, T> extends AbstractHttpMessageConverter<P> {

    private static final MediaType MEDIA_TYPE = new MediaType("text", "csv");

    private final Class<P> pageType;

    public AbstractCsvPageMessageConverter() {
        super(MEDIA_TYPE);
        pageType = ClassUtil.getGenericType(getClass());
    }

    protected boolean supports(Class<?> type) {

        return Objects.equals(pageType, type);
    }

    @Override
    protected P readInternal(Class<? extends P> pageType, HttpInputMessage inputMessage) throws HttpMessageNotReadableException {

        return null;
    }

    @SneakyThrows
    @Override
    protected void writeInternal(P page, HttpOutputMessage outputMessage) throws HttpMessageNotWritableException {

        var outputStream = new OutputStreamWriter(outputMessage.getBody());

        var beanToCsv = new StatefulBeanToCsvBuilder<T>(outputStream).build();

        beanToCsv.write(getContent(page));

        outputStream.close();
    }

    public List<T> getContent(P page) {

        return page.getContent();
    }
}
