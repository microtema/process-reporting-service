package de.microtema.monitoring.sql.service;

import de.microtema.monitoring.sql.model.SqlRequest;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.util.*;

@Service
@RequiredArgsConstructor
public class SqlService {

    private final DataSource dataSource;

    public Object execute(SqlRequest sqlData) {

        var sql = sqlData.getSql();

        try (var connection = dataSource.getConnection()) {

            var statement = connection.prepareStatement(sql);

            var resultSet = statement.executeQuery();

            var list = new ArrayList<>();

            while (resultSet.next()) {

                var metaData = resultSet.getMetaData();
                var columnCount = metaData.getColumnCount();

                var answer = new HashMap<String, Object>();

                for (int index = 1; index < columnCount + 1; index++) {

                    var columnName = metaData.getColumnName(index);
                    var columnTypeName = metaData.getColumnTypeName(index);

                    Object value = null;

                    switch (columnTypeName) {
                        case "CLOB" -> {
                            var clob = resultSet.getClob(index);
                            if (Objects.nonNull(clob)) {
                                var reader = clob.getCharacterStream();
                                if (Objects.nonNull(reader)) {
                                    value = IOUtils.toString(reader);
                                }
                            }
                        }
                        case "VARCHAR", "VARCHAR2", "CHAR" -> value = resultSet.getString(index);
                        case "BIGINT", "NUMBER", "INTEGER" -> value = resultSet.getInt(index);
                        case "BOOLEAN" -> value = resultSet.getBoolean(index);
                        case "TIMESTAMP" -> value = resultSet.getTimestamp(index);
                        default -> value = columnTypeName;
                    }

                    answer.put(columnName, Map.of(columnTypeName, Optional.ofNullable(value).orElse("")));
                }

                list.add(answer);
            }

            return list;

        } catch (Exception e) {
            return e;
        }
    }
}
