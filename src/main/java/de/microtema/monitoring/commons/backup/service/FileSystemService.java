package de.microtema.monitoring.commons.backup.service;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Service;

import jakarta.annotation.PostConstruct;
import java.io.File;

@Log4j2
@Service
public class FileSystemService {

    @Value("${reporting.file-system.backup-dir:}")
    private String backupFolderPath;

    @Value("${reporting.file-system.backup-dir}")
    private FileSystemResource backupFolder;

    private boolean enabled;

    @PostConstruct
    private void init() {

        enabled = StringUtils.isNotEmpty(backupFolderPath);

        if (!enabled) {

            log.info("Backup Folder is not configured!");
            return;
        }

        File backupFolderFile = backupFolder.getFile();

        if (!backupFolderFile.exists()) {
            try {
                enabled = backupFolderFile.mkdirs();
            } catch (Exception e) {
                log.warn("Unable to crete the backup directory " + backupFolder.getPath(), e);
                enabled = false;
                return;
            }
        }

        long size = FileUtils.sizeOfDirectory(backupFolderFile);

        String contentLength = FileUtils.byteCountToDisplaySize(size);

        log.info("Backup Folder {} [{}] is successfully configured.", backupFolder.getPath(), contentLength);
    }

    public File getBackupFolder() {

        if (!existsBackupFolder()) {
            return null;
        }

        return backupFolder.getFile();
    }

    public boolean existsBackupFolder() {

        return enabled;
    }
}
