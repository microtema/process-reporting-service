package de.microtema.monitoring.process.model;

import de.microtema.monitoring.commons.models.ProcessStatus;

import java.time.LocalDateTime;

public class ProcessInstance {

    private String id;

    private ProcessStatus instanceStatus;

    private String instanceDescription;

    private String errorMessage;

    private String instanceId;

    private String processBusinessKey;

    private String instanceName;

    private String definitionKey;

    private String starterId;

    private String referenceId;

    private String referenceType;

    private String referenceValue;

    private LocalDateTime instanceStartTime;

    private LocalDateTime instanceEndTime;

    private long duration;

    private int retryCount;

    private boolean hidden;

    private int chunkCount;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ProcessStatus getInstanceStatus() {
        return instanceStatus;
    }

    public void setInstanceStatus(ProcessStatus instanceStatus) {
        this.instanceStatus = instanceStatus;
    }

    public String getInstanceDescription() {
        return instanceDescription;
    }

    public void setInstanceDescription(String instanceDescription) {
        this.instanceDescription = instanceDescription;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public String getProcessBusinessKey() {
        return processBusinessKey;
    }

    public void setProcessBusinessKey(String processBusinessKey) {
        this.processBusinessKey = processBusinessKey;
    }

    public String getInstanceName() {
        return instanceName;
    }

    public void setInstanceName(String instanceName) {
        this.instanceName = instanceName;
    }

    public String getDefinitionKey() {
        return definitionKey;
    }

    public void setDefinitionKey(String definitionKey) {
        this.definitionKey = definitionKey;
    }

    public String getStarterId() {
        return starterId;
    }

    public void setStarterId(String starterId) {
        this.starterId = starterId;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public String getReferenceType() {
        return referenceType;
    }

    public void setReferenceType(String referenceType) {
        this.referenceType = referenceType;
    }

    public String getReferenceValue() {
        return referenceValue;
    }

    public void setReferenceValue(String referenceValue) {
        this.referenceValue = referenceValue;
    }

    public LocalDateTime getInstanceStartTime() {
        return instanceStartTime;
    }

    public void setInstanceStartTime(LocalDateTime instanceStartTime) {
        this.instanceStartTime = instanceStartTime;
    }

    public LocalDateTime getInstanceEndTime() {
        return instanceEndTime;
    }

    public void setInstanceEndTime(LocalDateTime instanceEndTime) {
        this.instanceEndTime = instanceEndTime;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public int getRetryCount() {
        return retryCount;
    }

    public void setRetryCount(int retryCount) {
        this.retryCount = retryCount;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public int getChunkCount() {
        return chunkCount;
    }

    public void setChunkCount(int chunkCount) {
        this.chunkCount = chunkCount;
    }
}
