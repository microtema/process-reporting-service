package de.microtema.monitoring.log.model;

public enum LogLevel {

    DEBUG("Debug"),
    INFO("Info"),
    WARN("Warning"),
    ERROR("Error"),
    ALL("All Statuses");

    private final String displayName;

    LogLevel(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {

        return displayName;
    }
}
