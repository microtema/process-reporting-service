package de.microtema.monitoring.definition.model;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

public class ProcessDefinitionPage extends PageImpl<ProcessDefinition> {

    public ProcessDefinitionPage(Page<ProcessDefinition> page) {
        super(page.getContent(), page.getPageable(), page.getTotalElements());
    }
}
