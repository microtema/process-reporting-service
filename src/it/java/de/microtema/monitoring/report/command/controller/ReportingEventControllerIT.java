package de.microtema.monitoring.report.command.controller;

import de.microtema.model.builder.annotation.Model;
import de.microtema.model.builder.enums.ModelType;
import de.microtema.model.builder.util.FieldInjectionUtil;
import de.microtema.monitoring.Application;
import de.microtema.monitoring.event.command.controller.ReportingEventController;
import de.microtema.monitoring.event.command.model.ReportEvent;
import de.microtema.monitoring.event.command.model.ReportStatus;
import de.microtema.monitoring.event.command.service.ProcessEventWriteService;
import de.microtema.monitoring.report.model.ProcessReport;
import jakarta.inject.Inject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {Application.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ReportingEventControllerIT {

    @Inject
    ReportingEventController sut;

    @Inject
    TestRestTemplate restTemplate;

    @Inject
    ProcessEventWriteService processEventWriteService;

    @Model(type = ModelType.MAX)
    ReportEvent reportEvent;

    String processReportId;

    String url = "/rest/api/report";

    @BeforeEach
    void setUp() {

        FieldInjectionUtil.injectFields(this);

        reportEvent.setStatus(ReportStatus.STARTED);
        reportEvent.setProcessVersion("1.0");

        processReportId = processEventWriteService.saveProcessReport(reportEvent);
    }

    @Test
    void getProcessReports() {

        url += "/transaction/" + reportEvent.getTransactionId() + "/" + reportEvent.getRetryCount();

        var response = restTemplate.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<List<ProcessReport>>() {
        });

        assertNotNull(response);
        assertSame(HttpStatus.OK, response.getStatusCode());

        var reports = response.getBody();
        assertNotNull(reports);
    }
}
