package de.microtema.monitoring.commons.converter;

import de.microtema.model.builder.util.FieldInjectionUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import jakarta.inject.Inject;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class BooleanConverterTest {

    @Inject
    BooleanConverter sut;

    @BeforeEach
    void setUp() {

        FieldInjectionUtil.injectFields(this);
    }

    @ParameterizedTest(name = "{index} => invoiceStatus={0}, @expected={1}")
    @CsvSource({
            "true, 1",
            "false, 0",
    })
    void convertToDatabaseColumn(boolean attribute, char dbData) {

        var answer = sut.convertToDatabaseColumn(attribute);

        assertNotNull(answer);
        assertEquals(dbData, answer);
    }

    @ParameterizedTest(name = "{index} => dbData={0}, @attribute={1}")
    @CsvSource({
            "1, true",
            "0, false",
    })
    void convertToEntityAttribute(char dbData, boolean attribute) {

        var answer = sut.convertToEntityAttribute(dbData);

        assertNotNull(answer);
        assertEquals(attribute, answer);
    }
}
