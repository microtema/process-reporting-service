package de.microtema.monitoring.timerange.model;

import lombok.Data;

@Data
public class TimeRangeEntry {

    private String value;

    private String displayName;
}
