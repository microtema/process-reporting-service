package de.microtema.monitoring.event.command.model;

public class RegisterEvent {

    /**
     * Camunda process diagram as String
     */
    private String processDiagram;

    /**
     * Process diagram file name
     */
    private String fileName;

    /**
     * Camunda process id
     */
    private String processId;

    /**
     * Camunda process version 1.0
     */
    private String processVersion;

    /**
     * Bounded Context of this event, [user, invoice, billing, ...]
     */
    private String boundedContext;

    /**
     * Retention time in days
     */
    private int retentionTime;

    public String getProcessDiagram() {
        return processDiagram;
    }

    public void setProcessDiagram(String processDiagram) {
        this.processDiagram = processDiagram;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getProcessId() {
        return processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }

    public String getProcessVersion() {
        return processVersion;
    }

    public void setProcessVersion(String processVersion) {
        this.processVersion = processVersion;
    }

    public String getBoundedContext() {
        return boundedContext;
    }

    public void setBoundedContext(String boundedContext) {
        this.boundedContext = boundedContext;
    }

    public int getRetentionTime() {
        return retentionTime;
    }

    public void setRetentionTime(int retentionTime) {
        this.retentionTime = retentionTime;
    }
}
