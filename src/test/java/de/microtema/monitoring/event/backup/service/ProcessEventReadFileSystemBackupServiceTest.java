package de.microtema.monitoring.event.backup.service;

import de.microtema.model.builder.ModelBuilderFactory;
import de.microtema.monitoring.commons.backup.model.BackupType;
import de.microtema.monitoring.definition.model.ProcessDefinition;
import de.microtema.monitoring.definition.service.ProcessDefinitionService;
import de.microtema.monitoring.event.backup.model.ProcessFileSystem;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ProcessEventReadFileSystemBackupServiceTest {

    @InjectMocks
    ProcessEventReadFileSystemBackupService sut;

    @Mock
    ProcessDefinitionService processDefinitionService;

    @Mock
    ProcessDefinition processDefinition;

    @Test
    void getBackupType() {

        assertEquals(BackupType.EVENT, sut.getBackupType());
    }

    @Test
    void getBackups() {

        var page = 0;
        var size = 25;
        var files = ModelBuilderFactory.list(ProcessFileSystem.class, 25);
        when(processDefinitionService.getProcessDefinitionByDefinitionKey(any())).thenReturn(processDefinition);

        var answer = sut.getBackups(page, size, files);

        assertNotNull(answer);
        assertEquals(1, answer.getTotalPages());
        assertEquals(files.size(), answer.getTotalElements());
        assertEquals(size, answer.getNumberOfElements());
        assertEquals(size, answer.getSize());
        assertEquals(files, answer.getContent());
    }

    @Test
    void getBackups_2() {

        var page = 0;
        var size = 25;
        var files = ModelBuilderFactory.list(ProcessFileSystem.class, 50);
        when(processDefinitionService.getProcessDefinitionByDefinitionKey(any())).thenReturn(processDefinition);

        var answer = sut.getBackups(page, size, files);

        assertNotNull(answer);
        assertEquals(2, answer.getTotalPages());
        assertEquals(files.size(), answer.getTotalElements());
        assertEquals(25, answer.getNumberOfElements());
        assertEquals(25, answer.getSize());
        assertEquals(files.subList(0, 25), answer.getContent());
    }

    @Test
    void getBackups_3() {

        var page = 0;
        var size = 25;
        var files = ModelBuilderFactory.list(ProcessFileSystem.class, 64);
        when(processDefinitionService.getProcessDefinitionByDefinitionKey(any())).thenReturn(processDefinition);

        var answer = sut.getBackups(page, size, files);

        assertNotNull(answer);
        assertEquals(3, answer.getTotalPages());
        assertEquals(files.size(), answer.getTotalElements());
        assertEquals(25, answer.getNumberOfElements());
        assertEquals(25, answer.getSize());
        assertEquals(files.subList(0, 25), answer.getContent());
    }

    @Test
    void getBackups_3_1() {

        var page = 1;
        var size = 25;
        var files = ModelBuilderFactory.list(ProcessFileSystem.class, 64);
        when(processDefinitionService.getProcessDefinitionByDefinitionKey(any())).thenReturn(processDefinition);

        var answer = sut.getBackups(page, size, files);

        assertNotNull(answer);
        assertEquals(3, answer.getTotalPages());
        assertEquals(files.size(), answer.getTotalElements());
        assertEquals(25, answer.getNumberOfElements());
        assertEquals(25, answer.getSize());
        assertEquals(files.subList(25, 50), answer.getContent());
    }

    @Test
    void getBackups_3_2() {

        var page = 2;
        var size = 25;
        var files = ModelBuilderFactory.list(ProcessFileSystem.class, 64);
        when(processDefinitionService.getProcessDefinitionByDefinitionKey(any())).thenReturn(processDefinition);

        var answer = sut.getBackups(page, size, files);

        assertNotNull(answer);
        assertEquals(3, answer.getTotalPages());
        assertEquals(files.size(), answer.getTotalElements());
        assertEquals(14, answer.getNumberOfElements());
        assertEquals(25, answer.getSize());
        assertEquals(files.subList(50, 64), answer.getContent());
    }
}
