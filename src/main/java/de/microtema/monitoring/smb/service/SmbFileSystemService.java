package de.microtema.monitoring.smb.service;

import de.microtema.monitoring.smb.config.FileSystemProperties;
import jcifs.smb1.smb1.NtlmPasswordAuthentication;
import jcifs.smb1.smb1.SmbException;
import jcifs.smb1.smb1.SmbFile;
import jcifs.smb1.smb1.SmbFileInputStream;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Log4j2
@Service
@RequiredArgsConstructor
public class SmbFileSystemService {

    private final FileSystemProperties fileSystemProperties;

    /**
     * Check if there exists files by given path
     *
     * @param folderName may not be null
     * @return boolean
     */
    public boolean exists(String folderName) {

        var smbFiles = readFilteredSmbFiles(folderName);

        return (smbFiles.length > 0);
    }

    /**
     * Read value consume and move to .success | .error folder
     *
     * @param consumer may not be null
     * @param <T>      generic type
     * @return generic type
     */
    public <T> T readValueAndMove(Function<InputStream, T> consumer) {

        return readValue(consumer, null, true);
    }

    /**
     * Read value consume without moving to .success | .error folder
     *
     * @param consumer may not be null
     * @param <T>      generic type
     * @return generic type
     */
    public <T> T readValue(Function<InputStream, T> consumer) {

        return readValue(consumer, null, false);
    }

    /**
     * Read value and consume without moving to .success | .error folder
     *
     * @param supplier   may not be null or empty
     * @param folderName may be null
     * @param <T>        generic type
     * @return generic type
     */
    public <T> T readValue(Function<InputStream, T> supplier, String folderName) {

        return readValue(supplier, folderName, false);
    }

    /**
     * @param supplier   may not be null or empty
     * @param folderName may be null
     * @param moveFile   move file after read to .success or .error folder
     * @param <T>        generic type
     * @return generic type
     */
    public <T> T readValue(Function<InputStream, T> supplier, String folderName, boolean moveFile) {

        var smbFiles = readFilteredSmbFiles(folderName);

        if (smbFiles.length == 0) {

            throw new IllegalStateException("There is any file provided at this time in Shared Folder " + getRootFolderPath(folderName) + "!");
        }

        var smbFile = smbFiles[0];

        try (var inputStream = createSmbFileInputStream(smbFile)) {

            T answer = supplier.apply(inputStream);

            if (moveFile) {
                // NOTE: to prevent reading the same file next time/call, we need to move the file to success folder
                moveFileToFolder(smbFile, ".success");
            }

            return answer;

        } catch (Exception e) {

            var message = "Unable to read from file system! " + smbFile.getPath();

            log.warn(message, e);

            if (moveFile) {
                // NOTE: We are not able to read the content of this file, so we have to move it to error folder
                // to prevent reading the same file nex time/call.
                moveFileToFolder(smbFile, ".error");
            }

            throw new IllegalStateException(message, e);
        }
    }

    protected SmbFileInputStream createSmbFileInputStream(SmbFile smbFile) throws SmbException, MalformedURLException, UnknownHostException {

        return new SmbFileInputStream(smbFile);
    }

    @SneakyThrows
    protected SmbFile[] readFilteredSmbFiles(String folderName) {

        var rootFolderPath = getRootFolderPath(folderName);
        var authentication = getNtlmPasswordAuthentication();

        var smbFolder = new SmbFile(rootFolderPath, authentication);

        Validate.isTrue(smbFolder.exists(), "Unable to read on " + rootFolderPath);

        return smbFolder.listFiles((smbFile, fileName) -> StringUtils.endsWith(fileName.toLowerCase(), ".csv"));
    }

    public String getRootFolderPath() {

        return getRootFolderPath(null);
    }

    public String getRootFolderPath(String folderName) {

        var remoteHost = fileSystemProperties.getServer();
        var remoteDir = fileSystemProperties.getPath();

        return "smb://" + createDirectoryPath(remoteHost, remoteDir, folderName);
    }

    protected String createDirectoryPath(String... paths) {

        return Stream.of(paths).map(StringUtils::trimToNull).filter(Objects::nonNull).map(it -> {
            it = StringUtils.removeStart(it, "/");
            it = StringUtils.removeEnd(it, "/");
            return it;
        }).collect(Collectors.joining("/")) + "/"; // directory must end with '/'
    }

    @SneakyThrows
    private void moveFileToFolder(SmbFile file, String folderName) {

        var authentication = getNtlmPasswordAuthentication();

        var targetFolderPath = createDirectoryPath(file.getParent(), folderName);

        var newFilePath = targetFolderPath + System.currentTimeMillis() + "-" + file.getName();

        log.info("Move {} to folder {}", file.getPath(), newFilePath);

        var targetFolder = new SmbFile(targetFolderPath, authentication);
        if (!targetFolder.exists()) {
            targetFolder.mkdir();
        }

        var newFile = new SmbFile(newFilePath, authentication);

        file.renameTo(newFile);
    }

    protected NtlmPasswordAuthentication getNtlmPasswordAuthentication() {

        var domain = fileSystemProperties.getDomain();
        var username = fileSystemProperties.getUsername();
        var password = fileSystemProperties.getPassword();

        return new NtlmPasswordAuthentication(domain, username, password);
    }
}
