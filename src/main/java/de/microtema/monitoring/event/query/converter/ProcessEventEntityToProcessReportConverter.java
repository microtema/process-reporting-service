package de.microtema.monitoring.event.query.converter;

import de.microtema.model.converter.Converter;
import de.microtema.model.converter.MetaConverter;
import de.microtema.monitoring.event.command.repository.ProcessEventEntity;
import de.microtema.monitoring.report.model.ProcessReport;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class ProcessEventEntityToProcessReportConverter implements Converter<ProcessReport, ProcessEventEntity> {


    @Override
    public void update(ProcessReport dest, ProcessEventEntity orig) {

        dest.setId(orig.getUuid());

        dest.setActivityId(orig.getReportId());
        dest.setDisplayName(orig.getReportName());
        dest.setDescription(orig.getReportDescription());

        dest.setMultipleInstanceIndex(Optional.ofNullable(orig.getMultipleInstanceIndex()).orElse("0.0"));

        dest.setProcessStatus(orig.getReportStatus());
        dest.setProcessBusinessKey(orig.getProcessBusinessKey());
        dest.setDefinitionKey(orig.getDefinitionKey());
        dest.setProcessName(orig.getInstanceName());

        dest.setErrorMessage(orig.getErrorMessage());
        dest.setInputPayload(orig.getInputPayload());
        dest.setOutputPayload(orig.getOutputPayload());

        dest.setReportStartTime(orig.getReportStartTime());
        dest.setReportEndTime(orig.getReportEndTime());
        dest.setDuration(getDuration(dest.getReportStartTime(), dest.getReportEndTime()));

        dest.setRetryCount(orig.getRetryCount());
    }

    private long getDuration(LocalDateTime instanceStartTime, LocalDateTime instanceEndTime) {

        if (Objects.isNull(instanceStartTime)) {
            return -1;
        }

        return ChronoUnit.MILLIS.between(instanceStartTime, Optional.ofNullable(instanceEndTime).orElse(LocalDateTime.now()));
    }
}
