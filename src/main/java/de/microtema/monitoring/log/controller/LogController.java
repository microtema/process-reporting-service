package de.microtema.monitoring.log.controller;

import de.microtema.monitoring.log.facade.LogFacade;
import de.microtema.monitoring.log.model.*;
import de.microtema.monitoring.timerange.model.TimeRangeType;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Log4j2
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/rest/api/log")
public class LogController {

    private final LogFacade facade;

    /**
     * This endpoint will be called by Integration Layer by given Event Medata data.
     *
     * @param event may not be null
     * @return ResponseEntity
     */
    @PostMapping
    public ResponseEntity<String> saveLogEvent(@RequestBody LogReportEvent event) {

        log.debug("The LogEvent event {} {} {} has been accepted for processing.", event.getRuntimeName(), event.getRuntimeId(), event.getLevel());

        return ResponseEntity.ok(facade.saveLogEvent(event));
    }

    @GetMapping
    public ResponseEntity<LogDataPage> getLogDataPage(
            @RequestParam(name = "page", required = false, defaultValue = "0") int page,
            @RequestParam(name = "size", required = false, defaultValue = "25") int size,
            @RequestParam(name = "properties", required = false, defaultValue = "logTimestamp") String properties,
            @RequestParam(name = "runtimeName", required = false) String runtimeName,
            @RequestParam(name = "query", required = false) String query,
            @RequestParam(name = "logLevel", required = false, defaultValue = "ALL") LogLevel logLevel,
            @RequestParam(name = "timeRange", required = false, defaultValue = "ALL") TimeRangeType timeRange,
            @RequestParam(name = "startDate", required = false) LocalDateTime startDate,
            @RequestParam(name = "endDate", required = false) LocalDateTime endDate) {

        return ResponseEntity.ok(new LogDataPage(facade.getLogDataPage(page, size, properties, query, runtimeName, logLevel, timeRange, startDate, endDate)));
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<LogData> getLogData(@PathVariable String id) {

        return ResponseEntity.ok(facade.getLogData(id));
    }

    @GetMapping(value = "/level")
    public ResponseEntity<List<LogLevelDTO>> getLogLevelList() {

        return ResponseEntity.ok(facade.getLogLevelList());
    }

    @GetMapping(value = "/runtime")
    public ResponseEntity<List<RuntimeData>> getRunTimes() {

        return ResponseEntity.ok(facade.getRunTimes());
    }
}
