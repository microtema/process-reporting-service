package de.microtema.monitoring.message.model;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

public class ProcessMessagePage extends PageImpl<ProcessMessage> {

    public ProcessMessagePage(Page<ProcessMessage> page) {
        super(page.getContent(), page.getPageable(), page.getTotalElements());
    }
}
