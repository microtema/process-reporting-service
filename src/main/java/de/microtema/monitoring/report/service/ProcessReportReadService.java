package de.microtema.monitoring.report.service;

import de.microtema.monitoring.commons.converter.PageRequestDTOToPageRequestConverter;
import de.microtema.monitoring.commons.models.PageRequestDTO;
import de.microtema.monitoring.commons.models.ProcessStatus;
import de.microtema.monitoring.report.converter.ProcessReportEntityToProcessReportConverter;
import de.microtema.monitoring.report.converter.ProcessReportSpecificationToQuerySpecificationConverter;
import de.microtema.monitoring.report.model.ProcessReport;
import de.microtema.monitoring.report.model.ProcessReportSpecification;
import de.microtema.monitoring.report.repository.ProcessReportRepository;
import de.microtema.monitoring.timerange.model.TimeRangeType;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ProcessReportReadService {

    private final ProcessReportRepository repository;
    private final PageRequestDTOToPageRequestConverter pageRequestConverter;
    private final ProcessReportEntityToProcessReportConverter processReportConverter;
    private final ProcessReportSpecificationToQuerySpecificationConverter specificationConverter;

    public Page<ProcessReport> getProcessReports(int page, int size, String properties, String query, String definitionKey, ProcessStatus reportStatus, TimeRangeType timeRange, LocalDateTime startDate, LocalDateTime endDate) {

        var processReportSpecification = ProcessReportSpecification.builder()
                .query(query)
                .definitionKey(definitionKey)
                .reportStatus(reportStatus)
                .timeRange(timeRange)
                .startDate(startDate)
                .endDate(endDate)
                .build();

        var pageRequestDTO = PageRequestDTO.of(page, size, properties);

        var specification = specificationConverter.convert(processReportSpecification);
        var pageable = pageRequestConverter.convert(pageRequestDTO);

        var entityPage = repository.findAll(specification, pageable);

        return entityPage.map(processReportConverter::convert);
    }

    public List<ProcessReport> getProcessReports(String processBusinessKey, int retryCount) {

        var reports = repository.findAllBy(processBusinessKey, retryCount);

        return processReportConverter.convertList(reports);
    }

    public ProcessReport getProcessReport(String uuid) {

        return repository.findById(uuid).map(processReportConverter::convert).orElse(null);
    }
}
