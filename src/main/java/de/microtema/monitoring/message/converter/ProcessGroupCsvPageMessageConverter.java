package de.microtema.monitoring.message.converter;

import de.microtema.monitoring.csv.converter.AbstractCsvPageMessageConverter;
import de.microtema.monitoring.message.model.ProcessMessage;
import de.microtema.monitoring.message.model.ProcessMessagePage;
import org.springframework.stereotype.Component;

@Component
public class ProcessGroupCsvPageMessageConverter extends AbstractCsvPageMessageConverter<ProcessMessagePage, ProcessMessage> {

}
