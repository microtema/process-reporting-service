package de.microtema.monitoring.commons.backup.service;

import de.microtema.monitoring.commons.backup.model.BackupType;
import de.microtema.monitoring.event.backup.model.ProcessFileSystem;
import de.microtema.monitoring.timerange.model.TimeRangeType;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Page;

import java.io.File;
import java.time.LocalDateTime;

@Log4j2
@RequiredArgsConstructor
public abstract class ProcessDeleteFileSystemBackupService {

    private final FileSystemService fileSystemService;
    private final ProcessReadFileSystemBackupService systemBackupService;

    public void deleteBackups(int page, int size, String query, String definitionKey, TimeRangeType timeRange, LocalDateTime startDate, LocalDateTime endDate) {

        if (!fileSystemService.existsBackupFolder()) {
            return;
        }

        var backups = systemBackupService.getBackups(page, size, query, definitionKey, timeRange, startDate, endDate);

        if (backups.isEmpty()) {
            return;
        }

        backups.getContent().forEach(this::deleteBackup);

        deleteBackups(page, size, query, definitionKey, timeRange, startDate, endDate);
    }

    public abstract BackupType getBackupType();

    private void deleteBackup(ProcessFileSystem processFileSystem) {

        var path = processFileSystem.getPath();

        var delete = new File(path).delete();

        log.info("Backup {} successfully deleted from {} FileSystem -> {}", path, getBackupType(), delete);
    }
}
