package de.microtema.monitoring.message.service;

import de.microtema.monitoring.commons.converter.PageRequestDTOToPageRequestConverter;
import de.microtema.monitoring.commons.converter.ProcessStatusToProcessStatusDTOConverter;
import de.microtema.monitoring.commons.models.PageRequestDTO;
import de.microtema.monitoring.commons.models.ProcessStatus;
import de.microtema.monitoring.commons.models.ProcessStatusDTO;
import de.microtema.monitoring.message.converter.ProcessMessageEntityToProcessMessageConverter;
import de.microtema.monitoring.message.converter.ProcessMessageSpecificationToSpecificationConverter;
import de.microtema.monitoring.message.model.ProcessMessage;
import de.microtema.monitoring.message.repository.ProcessMessageRepository;
import de.microtema.monitoring.process.model.ProcessInstanceSpecification;
import de.microtema.monitoring.timerange.model.TimeRangeType;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Transactional
@RequiredArgsConstructor
public class ProcessMessageReadService {

    private final List<ProcessStatus> processStatusList = Stream.of(ProcessStatus.values())
            .filter(it -> !it.is(ProcessStatus.PROCESS_COMPLETED))
            .collect(Collectors.toList());

    private final ProcessMessageRepository queryRepository;
    private final PageRequestDTOToPageRequestConverter pageRequestConverter;
    private final ProcessStatusToProcessStatusDTOConverter processStatusToProcessStatusDTOConverter;
    private final ProcessMessageSpecificationToSpecificationConverter specificationConverter;
    private final ProcessMessageEntityToProcessMessageConverter entityToProcessMessageConverter;

    public Page<ProcessMessage> getProcessMessages(int page, int size, String properties, String query, String definitionKey, ProcessStatus processStatus, TimeRangeType timeRange, LocalDateTime startDate, LocalDateTime endDate) {

        var pageRequestDTO = PageRequestDTO.of(page, size, properties);
        var pageable = pageRequestConverter.convert(pageRequestDTO);

        var processInstanceSpecification = ProcessInstanceSpecification.builder()
                .definitionKey(definitionKey)
                .processStatus(processStatus)
                .timeRange(timeRange)
                .startDate(startDate)
                .endDate(endDate)
                .query(query).build();

        var specification = specificationConverter.convert(processInstanceSpecification);

        var pageObject = queryRepository.findAll(specification, pageable);

        return pageObject.map(entityToProcessMessageConverter::convert);
    }

    public List<ProcessStatusDTO> getProcessMessageStatusList() {

        return processStatusToProcessStatusDTOConverter.convertList(processStatusList);
    }
}
