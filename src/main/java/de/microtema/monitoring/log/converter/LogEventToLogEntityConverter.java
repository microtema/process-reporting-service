package de.microtema.monitoring.log.converter;

import de.microtema.model.converter.Converter;
import de.microtema.monitoring.log.model.LogLevel;
import de.microtema.monitoring.log.model.LogReportEvent;
import de.microtema.monitoring.log.repository.LogEntity;
import org.springframework.stereotype.Component;

@Component
public class LogEventToLogEntityConverter implements Converter<LogEntity, LogReportEvent> {

    @Override
    public void update(LogEntity dest, LogReportEvent orig) {

        dest.setRuntimeId(orig.getRuntimeId());
        dest.setRuntimeName(orig.getRuntimeName());

        dest.setLogMessage(orig.getMessage());
        dest.setStackTrace(orig.getStackTrace());

        dest.setLogLevel(LogLevel.valueOf(orig.getLevel()));
        dest.setLogTimestamp(orig.getTimestamp());
    }
}
