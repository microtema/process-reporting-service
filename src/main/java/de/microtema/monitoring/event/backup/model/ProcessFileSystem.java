package de.microtema.monitoring.event.backup.model;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.Comparator;

@Data
public class ProcessFileSystem implements Comparable<ProcessFileSystem> {

    private String definitionKey;

    private String instanceName;

    private LocalDateTime creationTime;

    private int page;

    private int elementCount;

    private String path;

    @Override
    public int compareTo(ProcessFileSystem o) {

        return Comparator.comparing(ProcessFileSystem::getCreationTime)
                .thenComparingInt(ProcessFileSystem::getPage)
                .compare(this, o);
    }
}
