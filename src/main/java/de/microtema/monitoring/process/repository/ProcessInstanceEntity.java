package de.microtema.monitoring.process.repository;


import de.microtema.monitoring.commons.models.ProcessStatus;
import de.microtema.monitoring.commons.repository.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import jakarta.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@EqualsAndHashCode(callSuper = false)
@Table(name = "PROCESS_INSTANCE_VIEW")
public class ProcessInstanceEntity extends BaseEntity {

    /**
     * definitionKey is set of process-id, version and deployment-id
     */
    @Column(name = "DEFINITION_KEY")
    private String definitionKey;

    /**
     * instanceName is needed for query search
     */
    @Column(name = "INSTANCE_NAME")
    private String instanceName;

    @Enumerated(value = EnumType.ORDINAL)
    @Column(name = "INSTANCE_STATUS")
    private ProcessStatus instanceStatus;

    @Column(name = "INSTANCE_DESCRIPTION")
    private String instanceDescription;

    @Column(columnDefinition = "Text", name = "ERROR_MESSAGE")
    private String errorMessage;

    /**
     * InstanceId may be null at initial and it will be set from process engine
     */
    @Column(name = "INSTANCE_ID")
    private String instanceId;

    /**
     * Process BusinessKey it will be generated from system and passed to process engine
     */
    @Column(name = "PROCESS_BUSINESS_KEY")
    private String processBusinessKey;

    @Column(name = "STARTER_ID")
    private String starterId;

    @Column(name = "REFERENCE_ID")
    private String referenceId;

    @Column(name = "REFERENCE_TYPE")
    private String referenceType;

    @Column(name = "REFERENCE_VALUE")
    private String referenceValue;

    @Column(name = "INSTANCE_START_TIME")
    private LocalDateTime instanceStartTime;

    @Column(name = "INSTANCE_END_TIME")
    private LocalDateTime instanceEndTime;

    @Column(name = "RETRY_COUNT")
    private int retryCount;
}
