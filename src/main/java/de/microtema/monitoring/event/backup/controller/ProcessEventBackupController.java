package de.microtema.monitoring.event.backup.controller;

import de.microtema.monitoring.event.backup.facade.ProcessEventBackupFacade;
import de.microtema.monitoring.event.backup.model.ProcessEventFileSystemPage;
import de.microtema.monitoring.event.backup.model.ProcessFileSystem;
import de.microtema.monitoring.timerange.model.TimeRangeType;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.Map;

@Log4j2
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/rest/api/process/backup/file-system")
public class ProcessEventBackupController {

    private final ProcessEventBackupFacade facade;

    @GetMapping
    public ResponseEntity<Page<ProcessFileSystem>> getProcessBackups(
            @RequestParam(name = "page", required = false, defaultValue = "0") int page,
            @RequestParam(name = "size", required = false, defaultValue = "25") int size,
            @RequestParam(name = "query", required = false) String query,
            @RequestParam(name = "definitionKey", required = false) String definitionKey,
            @RequestParam(name = "timeRange", required = false, defaultValue = "ALL") TimeRangeType timeRange,
            @RequestParam(name = "startDate", required = false) LocalDateTime startDate,
            @RequestParam(name = "endDate", required = false) LocalDateTime endDate) {

        return ResponseEntity.ok(new ProcessEventFileSystemPage(facade.getProcessBackups(page, size, query, definitionKey, timeRange, startDate, endDate)));
    }

    @PostMapping
    public ResponseEntity<Map<String, Boolean>> executeProcessBackups(
            @RequestParam(name = "page", required = false, defaultValue = "0") int page,
            @RequestParam(name = "size", required = false, defaultValue = "25") int size,
            @RequestParam(name = "query", required = false) String query,
            @RequestParam(name = "definitionKey", required = false) String definitionKey,
            @RequestParam(name = "timeRange", required = false, defaultValue = "ALL") TimeRangeType timeRange,
            @RequestParam(name = "startDate", required = false) LocalDateTime startDate,
            @RequestParam(name = "endDate", required = false) LocalDateTime endDat) {

        return ResponseEntity.ok(Map.of("status", facade.restoreProcessBackups(page, size, query, definitionKey, timeRange, startDate, endDat)));
    }

    @DeleteMapping
    public ResponseEntity<Map<String, Boolean>> deleteProcessBackups(
            @RequestParam(name = "page", required = false, defaultValue = "0") int page,
            @RequestParam(name = "size", required = false, defaultValue = "25") int size,
            @RequestParam(name = "query", required = false) String query,
            @RequestParam(name = "definitionKey", required = false) String definitionKey,
            @RequestParam(name = "timeRange", required = false, defaultValue = "ALL") TimeRangeType timeRange,
            @RequestParam(name = "startDate", required = false) LocalDateTime startDate,
            @RequestParam(name = "endDate", required = false) LocalDateTime endDat) {

        return ResponseEntity.ok(Map.of("status", facade.deleteProcessBackups(page, size, query, definitionKey, timeRange, startDate, endDat)));
    }
}
