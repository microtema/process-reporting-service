package de.microtema.monitoring.event.command.controller;

import de.microtema.monitoring.event.command.facade.ProcessEventFacade;
import de.microtema.monitoring.event.command.model.ReportEvent;
import de.microtema.monitoring.report.model.ProcessReport;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Log4j2
@RestController
@RequiredArgsConstructor
@RequestMapping("/rest/api/report")
public class ReportingEventController {

    private final ProcessEventFacade facade;

    /**
     * This endpoint will be called by Integration Layer by given Event Medata data.
     *
     * @param event may not be null
     * @return ResponseEntity
     */
    @PostMapping
    public ResponseEntity<ProcessReport> saveReportEvent(@RequestBody ReportEvent event) {

        log.debug("The ReportEvent event {} {} {} {} for process {} has been accepted for processing.", event.getElementId(), event.getStatus(), event.getTransactionId(), event.getRetryCount(), event.getProcessId());

        return ResponseEntity.ok(facade.saveReportEvent(event));
    }
}
