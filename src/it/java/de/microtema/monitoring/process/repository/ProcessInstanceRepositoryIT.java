package de.microtema.monitoring.process.repository;

import de.microtema.model.builder.annotation.Model;
import de.microtema.model.builder.enums.ModelType;
import de.microtema.model.builder.util.FieldInjectionUtil;
import de.microtema.monitoring.Application;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import jakarta.inject.Inject;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Transactional
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {Application.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ProcessInstanceRepositoryIT {

    @Inject
    ProcessInstanceRepository sut;

    @Model(type = ModelType.MAX)
    ProcessInstanceEntity model;

    @BeforeEach
    void setUp() {
        FieldInjectionUtil.injectFields(this);
        model = sut.save(model);
    }

    @Test
    void findAll() {

        var answer = sut.findAll();

        assertEquals(1, answer.size());
        assertTrue(answer.contains(model));
    }

    @Test
    void getOne() {

        var answer = sut.getReferenceById(model.getUuid());

        assertEquals(model, answer);
    }
}
