package de.microtema.monitoring.message.repository;

import de.microtema.monitoring.commons.repository.BaseEntityRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProcessMessageRepository extends BaseEntityRepository<ProcessMessageEntity> {

}
