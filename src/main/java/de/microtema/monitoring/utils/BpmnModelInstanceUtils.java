package de.microtema.monitoring.utils;

import de.microtema.model.converter.util.CollectionUtil;
import lombok.experimental.UtilityClass;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.camunda.bpm.model.bpmn.Bpmn;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.bpmn.instance.Process;
import org.camunda.bpm.model.bpmn.instance.*;
import org.camunda.bpm.model.bpmn.instance.camunda.CamundaProperties;
import org.camunda.bpm.model.bpmn.instance.camunda.CamundaProperty;
import org.camunda.bpm.model.xml.instance.ModelElementInstance;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@UtilityClass
public class BpmnModelInstanceUtils {

    public static String getExtensionPropertyValue(FlowElement flowElement, String propertyName) {

        CamundaProperty camundaProperty = getCamundaProperty(flowElement.getExtensionElements(), propertyName);

        return Optional.ofNullable(camundaProperty).map(CamundaProperty::getCamundaValue).orElse(null);
    }

    public static String getExtensionPropertyValue(BaseElement baseElement, String propertyName) {

        CamundaProperty camundaProperty = getCamundaProperty(baseElement.getExtensionElements(), propertyName);

        return Optional.ofNullable(camundaProperty).map(CamundaProperty::getCamundaValue).orElse(null);
    }

    public static String getDescription(Process process) {

        return process.getDocumentations()
                .stream()
                .map(ModelElementInstance::getTextContent)
                .filter(StringUtils::isNotEmpty)
                .findFirst()
                .orElse(process.getName());
    }

    public static BpmnModelInstance getBpmnModelInstance(String bpmn) {

        var inputStream = new ByteArrayInputStream(bpmn.getBytes(StandardCharsets.UTF_8));

        return Bpmn.readModelFromStream(inputStream);
    }

    public static Integer getMajorVersion(String processVersion) {

        return Integer.parseInt(processVersion.split("\\.")[0]);
    }

    public static Integer getMinorVersion(String processVersion) {

        return Integer.parseInt(processVersion.split("\\.")[1]);
    }

    private static CamundaProperty getCamundaProperty(ExtensionElements extensionElements, String propertyName) {

        if (Objects.isNull(extensionElements)) {
            return null;
        }

        List<CamundaProperties> camundaProperties = extensionElements.getElementsQuery().filterByType(CamundaProperties.class).list();

        return camundaProperties.stream()
                .map(CamundaProperties::getCamundaProperties)
                .filter(CollectionUtils::isNotEmpty)
                .flatMap(Collection::stream)
                .filter(it -> StringUtils.equalsIgnoreCase(it.getCamundaName(), propertyName)).findFirst().orElse(null);
    }

    public static Process getProcess(BpmnModelInstance bpmnModelInstance, String definitionKey) {

        if (Objects.isNull(bpmnModelInstance)) {

            return null;
        }

        if (StringUtils.isEmpty(definitionKey)) {

            var elements = bpmnModelInstance.getModelElementsByType(Process.class);

            return CollectionUtil.first(elements);
        }

        return bpmnModelInstance.getModelElementById(definitionKey);
    }

    public static String getExtensionPropertyValue(BpmnModelInstance bpmnModelInstance, String elementId, String keyExpression) {

        if (Objects.isNull(bpmnModelInstance)) {
            return null;
        }

        BaseElement element = bpmnModelInstance.getModelElementById(elementId);

        return getExtensionPropertyValue(element, keyExpression);
    }

    public static int getHistoryTimeToLive(BpmnModelInstance bpmnModelInstance, int defaultValue) {

        var processes = bpmnModelInstance.getModelElementsByType(Process.class);

        var process = CollectionUtil.first(processes);

        var timeToLiveString = process.getCamundaHistoryTimeToLiveString();

        if (StringUtils.isEmpty(timeToLiveString)) {
            return defaultValue;
        }

        return Integer.parseInt(timeToLiveString);
    }

    public static String getVersionTag(BpmnModelInstance bpmnModelInstance, String processId, String processVersion) {

        var modelElementsByType = bpmnModelInstance.getModelElementsByType(Participant.class);

        var processes = modelElementsByType.stream().map(Participant::getProcess);

        if (StringUtils.isNotEmpty(processId)) {

            processes = processes.filter(it -> StringUtils.equalsIgnoreCase(it.getId(), processId));
        }

        return processes.map(Process::getCamundaVersionTag).filter(Objects::nonNull).findFirst().orElse(processVersion);
    }

    public static String getBoundedContext(Process baseElement) {

        CamundaProperty camundaProperty = getCamundaProperty(baseElement.getExtensionElements(), "boundedContext");

        return Optional.ofNullable(camundaProperty).map(CamundaProperty::getCamundaValue).orElse(null);
    }
}
