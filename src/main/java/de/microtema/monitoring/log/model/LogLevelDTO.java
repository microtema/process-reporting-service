package de.microtema.monitoring.log.model;

import lombok.Data;

@Data
public class LogLevelDTO {

    private String displayName;

    private String value;
}
