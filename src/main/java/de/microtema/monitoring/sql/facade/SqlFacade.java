package de.microtema.monitoring.sql.facade;

import de.microtema.monitoring.sql.model.SqlRequest;
import de.microtema.monitoring.sql.service.SqlService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class SqlFacade {

    private final SqlService sqlService;

    public Object execute(SqlRequest sql) {

        return sqlService.execute(sql);
    }
}
