package de.microtema.monitoring.commons.service;

import de.microtema.monitoring.commons.converter.ProcessStatusToProcessStatusDTOConverter;
import de.microtema.monitoring.commons.models.ProcessStatus;
import de.microtema.monitoring.commons.models.ProcessStatusDTO;
import de.microtema.monitoring.process.repository.ProcessInstanceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProcessStatusService {

    private final List<ProcessStatus> defaultProcessStatusList = List.of(ProcessStatus.STARTED, ProcessStatus.COMPLETED, ProcessStatus.ERROR);

    private final ProcessInstanceRepository processInstanceRepository;
    private final ProcessStatusToProcessStatusDTOConverter processStatusToProcessStatusDTOConverter;

    public List<ProcessStatusDTO> getProcessStatusList() {

        var savedProcessStatusSet = processInstanceRepository.getProcessStatus()
                .stream()
                .map(it -> ProcessStatus.fromOrdinal(it.getProcessStatus()))
                .filter(it -> !it.is(ProcessStatus.PROCESS_COMPLETED))
                .collect(Collectors.toSet());

        var uniqueProcessStatusList = new HashSet<>(defaultProcessStatusList);

        uniqueProcessStatusList.addAll(savedProcessStatusSet);
        uniqueProcessStatusList.add(ProcessStatus.ALL);

        var sortedProcessStatusList = uniqueProcessStatusList
                .stream()
                .sorted(Comparator.comparing(ProcessStatus::ordinal))
                .collect(Collectors.toList());

        return processStatusToProcessStatusDTOConverter.convertList(sortedProcessStatusList);
    }
}
