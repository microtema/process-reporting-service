package de.microtema.monitoring.event.command.repository;

import de.microtema.monitoring.commons.repository.BaseEntityRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface ProcessEventRepository extends BaseEntityRepository<ProcessEventEntity> {

    default List<ProcessEventEntity> findAllBy(String definitionKey, String businessKey, int retryCount) {

        return findAllByDefinitionKeyAndProcessBusinessKeyAndRetryCountOrderByReportStartTime(definitionKey, businessKey, retryCount);
    }

    List<ProcessEventEntity> findAllByDefinitionKeyAndProcessBusinessKeyAndRetryCountOrderByReportStartTime(String definitionKey, String businessKey, int retryCount);

    @Modifying
    @Transactional
    int deleteAllByDefinitionKeyAndProcessBusinessKeyAndRetryCount(String definitionKey, String businessKey, int retryCount);
}
