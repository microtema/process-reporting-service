package de.microtema.monitoring.process.controller;

import de.microtema.monitoring.commons.models.ProcessStatus;
import de.microtema.monitoring.commons.models.ProcessStatusDTO;
import de.microtema.monitoring.process.facade.ProcessInstanceFacade;
import de.microtema.monitoring.process.model.ProcessInstance;
import de.microtema.monitoring.process.model.ProcessInstancePage;
import de.microtema.monitoring.timerange.model.TimeRangeType;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@Log4j2
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/rest/api/process")
public class ProcessInstanceController {

    private final ProcessInstanceFacade facade;

    @GetMapping
    public ResponseEntity<Page<ProcessInstance>> getProcesses(
            @RequestParam(name = "page", required = false, defaultValue = "0") int page,
            @RequestParam(name = "size", required = false, defaultValue = "25") int size,
            @RequestParam(name = "properties", required = false, defaultValue = "instanceStartTime") String properties,
            @RequestParam(name = "query", required = false) String query,
            @RequestParam(name = "definitionKey", required = false) String definitionKey,
            @RequestParam(name = "status", required = false, defaultValue = "ALL") ProcessStatus processStatus,
            @RequestParam(name = "timeRange", required = false, defaultValue = "ALL") TimeRangeType timeRange,
            @RequestParam(name = "startDate", required = false) LocalDateTime startDate,
            @RequestParam(name = "endDate", required = false) LocalDateTime endDate) {

        return ResponseEntity.ok(new ProcessInstancePage(facade.getProcesses(page, size, properties, query, definitionKey, processStatus, timeRange, startDate, endDate)));
    }

    @GetMapping(value = "/businessKey/{key}/{retryCount}")
    public ResponseEntity<ProcessInstance> findProcessInstance(@PathVariable String key, @PathVariable(required = false) int retryCount) {

        return ResponseEntity.ok(facade.findProcessInstance(key, retryCount));
    }

    @GetMapping(value = "/status")
    public ResponseEntity<List<ProcessStatusDTO>> getProcessInstanceStatusList() {

        return ResponseEntity.ok(facade.getProcessInstanceStatusList());
    }
}
