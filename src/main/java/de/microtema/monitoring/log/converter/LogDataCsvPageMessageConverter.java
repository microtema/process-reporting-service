package de.microtema.monitoring.log.converter;

import de.microtema.monitoring.csv.converter.AbstractCsvPageMessageConverter;
import de.microtema.monitoring.log.model.LogData;
import de.microtema.monitoring.log.model.LogDataPage;
import org.springframework.stereotype.Component;

@Component
public class LogDataCsvPageMessageConverter extends AbstractCsvPageMessageConverter<LogDataPage, LogData> {

}
