package de.microtema.monitoring.definition.repository;


import de.microtema.monitoring.commons.repository.BaseEntityRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProcessDefinitionRepository extends BaseEntityRepository<ProcessDefinitionEntity> {

    @Override
    default Optional<ProcessDefinitionEntity> findOne(ProcessDefinitionEntity entity) {

        String definitionKey = entity.getDefinitionKey();
        Integer definitionMajorVersion = entity.getDefinitionMajorVersion();

        return findByDefinitionKeyAndDefinitionMajorVersionOrderByDefinitionVersionDesc(definitionKey, definitionMajorVersion);
    }

    default Optional<ProcessDefinitionEntity> findOne(String definitionKey, int majorVersion) {
        return findByDefinitionKeyAndDefinitionMajorVersionOrderByDefinitionVersionDesc(definitionKey, majorVersion);
    }

    Optional<ProcessDefinitionEntity> findByDefinitionKeyAndDefinitionMajorVersionOrderByDefinitionVersionDesc(String definitionKey, int majorVersion);

    Optional<ProcessDefinitionEntity> findByDefinitionKey(String definitionKey);
}
