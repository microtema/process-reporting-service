package de.microtema.monitoring.log.repository;

import de.microtema.monitoring.commons.repository.BaseEntityRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface LogRepository extends BaseEntityRepository<LogEntity> {

    /**
     * Delete all log with timestamp > 90 days.
     *
     * @return Integer
     */
    @Modifying
    @Transactional
    @Query(value = "DELETE FROM LOG_DATA p WHERE p.RUNTIME_NAME = ?1 AND p.LOG_TIMESTAMP < TRUNC(SYSDATE) - ?2", nativeQuery = true)
    int cleanup(@Param(value = "runtimeName") String runtimeName, @Param(value = "retentionDays") int retentionDays);

    @Query(value = "SELECT DISTINCT RUNTIME_NAME FROM LOG_DATA", nativeQuery = true)
    List<String> findAllRuntimeNames();
}
