package de.microtema.monitoring.report.converter;

import de.microtema.model.converter.Converter;
import de.microtema.monitoring.commons.models.ProcessStatus;
import de.microtema.monitoring.report.model.ProcessReportSpecification;
import de.microtema.monitoring.report.repository.ProcessReportEntity;
import de.microtema.monitoring.timerange.model.TimeRangeType;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.Path;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Component
public class ProcessReportSpecificationToQuerySpecificationConverter implements Converter<Specification<ProcessReportEntity>, ProcessReportSpecification> {

    @Override
    public Specification<ProcessReportEntity> convert(ProcessReportSpecification orig) {

        return (root, criteriaQuery, criteriaBuilder) -> {

            var predicate = getEqualsPredicate(root, criteriaBuilder, "definitionKey", orig.getDefinitionKey());
            var reportStatusPredicate = getStatusPredicate(root, criteriaBuilder, "reportStatus", orig.getReportStatus());
            var timeRangePredicate = getTimeRangePredicate(root, criteriaBuilder, orig.getTimeRange(), orig.getStartDate(), orig.getEndDate());

            predicate = criteriaBuilder.and(predicate, reportStatusPredicate);

            if (Objects.nonNull(timeRangePredicate)) {

                predicate = criteriaBuilder.and(predicate, timeRangePredicate);
            }

            var orPredicate = getOrPredicate(orig, root, criteriaBuilder);

            if (Objects.nonNull(orPredicate)) {

                predicate = criteriaBuilder.and(predicate, orPredicate);
            }

            return criteriaBuilder.and(predicate);
        };
    }

    private Predicate getTimeRangePredicate(Root<ProcessReportEntity> root, CriteriaBuilder criteriaBuilder, TimeRangeType timeRange, LocalDateTime startDate, LocalDateTime endDate) {

        var startDateTime = timeRange.getStartDateTime(startDate);
        var endDateTime = timeRange.getEndDateTime(endDate);

        return criteriaBuilder.between(root.get("reportStartTime"), startDateTime, endDateTime);
    }

    private Predicate getOrPredicate(ProcessReportSpecification orig, Root<ProcessReportEntity> root, CriteriaBuilder criteriaBuilder) {

        List<Predicate> predicates = new ArrayList<>();

        Predicate predicate = getLikePredicate(root, criteriaBuilder, "processBusinessKey", orig.getQuery());
        CollectionUtils.addIgnoreNull(predicates, predicate);

        predicate = getLikePredicate(root, criteriaBuilder, "reportName", orig.getQuery());
        CollectionUtils.addIgnoreNull(predicates, predicate);

        if (CollectionUtils.isEmpty(predicates)) {
            return null;
        }

        return criteriaBuilder.or(predicates.toArray(new Predicate[0]));
    }

    private Predicate getEqualsPredicate(Root<ProcessReportEntity> root, CriteriaBuilder criteriaBuilder, String property, String query) {

        if (StringUtils.isEmpty(query)) {
            return criteriaBuilder.and();
        }

        Path<String> path = root.get(property);

        return criteriaBuilder.equal(criteriaBuilder.lower(path), query.toLowerCase());
    }

    private Predicate getStatusPredicate(Root<ProcessReportEntity> root, CriteriaBuilder criteriaBuilder, String property, ProcessStatus processStatus) {

        if (ProcessStatus.ALL == processStatus || Objects.isNull(processStatus)) {
            return criteriaBuilder.and();
        }

        Path<ProcessStatus> path = root.get(property);

        return criteriaBuilder.equal(path, processStatus);
    }

    private Predicate getLikePredicate(Root<ProcessReportEntity> root, CriteriaBuilder criteriaBuilder, String property, String query) {

        if (StringUtils.isEmpty(query)) {
            return null;
        }

        Path<String> path = root.get(property);

        return criteriaBuilder.like(criteriaBuilder.lower(path), "%" + query.toLowerCase() + "%");
    }
}
