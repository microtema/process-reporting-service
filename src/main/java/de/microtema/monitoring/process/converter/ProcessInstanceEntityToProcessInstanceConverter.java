package de.microtema.monitoring.process.converter;

import de.microtema.model.converter.Converter;
import de.microtema.monitoring.process.model.ProcessInstance;
import de.microtema.monitoring.process.repository.ProcessInstanceEntity;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Objects;
import java.util.Optional;

@Component
public class ProcessInstanceEntityToProcessInstanceConverter implements Converter<ProcessInstance, ProcessInstanceEntity> {

    @Override
    public void update(ProcessInstance dest, ProcessInstanceEntity orig) {

        dest.setId(orig.getUuid());

        dest.setProcessBusinessKey(orig.getProcessBusinessKey());
        dest.setInstanceStatus(orig.getInstanceStatus());
        dest.setInstanceDescription(orig.getInstanceDescription());
        dest.setErrorMessage(orig.getErrorMessage());

        dest.setStarterId(orig.getStarterId());

        dest.setReferenceType(orig.getReferenceType());
        dest.setReferenceId(orig.getReferenceId());
        dest.setReferenceValue(orig.getReferenceValue());

        dest.setInstanceStartTime(orig.getInstanceStartTime());
        dest.setInstanceEndTime(orig.getInstanceEndTime());

        dest.setDuration(getDuration(dest.getInstanceStartTime(), dest.getInstanceEndTime()));

        dest.setDefinitionKey(orig.getDefinitionKey());
        dest.setInstanceName(orig.getInstanceName());

        dest.setRetryCount(orig.getRetryCount());
    }

    private long getDuration(LocalDateTime instanceStartTime, LocalDateTime instanceEndTime) {

        if (Objects.isNull(instanceStartTime)) {
            return -1;
        }

        return instanceStartTime.until(Optional.ofNullable(instanceEndTime).orElse(LocalDateTime.now()), ChronoUnit.MILLIS);
    }
}
