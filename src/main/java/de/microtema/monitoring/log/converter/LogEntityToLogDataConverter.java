package de.microtema.monitoring.log.converter;

import de.microtema.model.converter.Converter;
import de.microtema.monitoring.log.model.LogData;
import de.microtema.monitoring.log.repository.LogEntity;
import org.springframework.stereotype.Component;

@Component
public class LogEntityToLogDataConverter implements Converter<LogData, LogEntity> {

    @Override
    public void update(LogData dest, LogEntity orig) {

        dest.setId(orig.getUuid());

        dest.setRuntimeName(orig.getRuntimeName());
        dest.setRuntimeId(orig.getRuntimeId());

        dest.setMessage(orig.getLogMessage());
        dest.setStackTrace(orig.getStackTrace());

        dest.setLevel(orig.getLogLevel());
        dest.setTimestamp(orig.getLogTimestamp());
    }
}
