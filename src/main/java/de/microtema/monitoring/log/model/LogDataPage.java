package de.microtema.monitoring.log.model;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

public class LogDataPage extends PageImpl<LogData> {

    public LogDataPage(Page<LogData> page) {
        super(page.getContent(), page.getPageable(), page.getTotalElements());
    }
}
