package de.microtema.monitoring.event.backup.facade;

import de.microtema.monitoring.event.backup.model.ProcessFileSystem;
import de.microtema.monitoring.event.backup.service.ProcessEventDeleteFileSystemBackupService;
import de.microtema.monitoring.event.backup.service.ProcessEventReadFileSystemBackupService;
import de.microtema.monitoring.event.backup.service.ProcessEventRestoreFileSystemBackupService;
import de.microtema.monitoring.timerange.model.TimeRangeType;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@RequiredArgsConstructor
public class ProcessEventBackupFacade {

    private final ProcessEventReadFileSystemBackupService processInstanceReadFileSystemBackupService;
    private final ProcessEventRestoreFileSystemBackupService processEventRestoreFileSystemBackupService;
    private final ProcessEventDeleteFileSystemBackupService processEventDeleteFileSystemBackupService;

    public Page<ProcessFileSystem> getProcessBackups(int page, int size, String query, String definitionKey, TimeRangeType timeRange, LocalDateTime startDate, LocalDateTime endDate) {

        return processInstanceReadFileSystemBackupService.getBackups(page, size, query, definitionKey, timeRange, startDate, endDate);
    }

    public boolean restoreProcessBackups(int page, int size, String query, String definitionKey, TimeRangeType timeRange, LocalDateTime startDate, LocalDateTime endDate) {

        processEventRestoreFileSystemBackupService.restore(page, size, query, definitionKey, timeRange, startDate, endDate);

        return true;
    }

    public boolean deleteProcessBackups(int page, int size, String query, String definitionKey, TimeRangeType timeRange, LocalDateTime startDate, LocalDateTime endDate) {

        processEventDeleteFileSystemBackupService.deleteBackups(page, size, query, definitionKey, timeRange, startDate, endDate);

        return true;
    }
}
