package de.microtema.monitoring.message.model;

import de.microtema.monitoring.commons.models.ProcessStatus;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ProcessMessage {

    private String definitionKey;

    private String instanceName;

    private String instanceDescription;

    private String errorMessage;

    private String errorMessageHash;

    private ProcessStatus instanceStatus;

    private LocalDateTime instanceStartTime;

    private LocalDateTime instanceEndTime;

    private int totalCount;
}
