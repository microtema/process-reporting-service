package de.microtema.monitoring.definition.controller;

import de.microtema.model.builder.annotation.Model;
import de.microtema.model.builder.util.FieldInjectionUtil;
import de.microtema.monitoring.Application;
import de.microtema.monitoring.definition.model.ProcessDefinition;
import de.microtema.monitoring.definition.model.RegisterEventModelBuilder;
import de.microtema.monitoring.event.command.model.RegisterEvent;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import jakarta.inject.Inject;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {Application.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ProcessDefinitionControllerIT {

    @Inject
    ProcessDefinitionController sut;

    @Inject
    TestRestTemplate restTemplate;

    @Inject
    RegisterEventModelBuilder builder;

    @Model
    RegisterEvent registerEvent;

    String url = "/rest/api/definition";

    @BeforeEach
    void setUp() {
        FieldInjectionUtil.injectFields(this);
    }

    @Test
    void registerProcessDefinition() {

        // Given

        // When
        var response = restTemplate.postForEntity(url, registerEvent, ProcessDefinition.class);

        // Then
        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());

        var processDefinition = response.getBody();

        assertNotNull(processDefinition);
        assertEquals(registerEvent.getProcessId(), processDefinition.getDefinitionKey());
    }

    @Test
    void saveLastHeartBeatProcessDefinition() {

        // Given
        url += "/heart-beat";

        // When
        var response = restTemplate.postForEntity(url, registerEvent, ProcessDefinition.class);

        // Then
        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void processDefinitions() {

        url += "?page=0&size=10&query=foo&properties=!definitionName";

        var response = restTemplate.exchange(url, HttpMethod.GET, null, String.class);

        assertNotNull(response);
        assertSame(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
    }
}
