package de.microtema.monitoring.event.backup.model;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

public class ProcessEventFileSystemPage extends PageImpl<ProcessFileSystem> {

    public ProcessEventFileSystemPage(Page<ProcessFileSystem> page) {
        super(page.getContent(), page.getPageable(), page.getTotalElements());
    }
}
