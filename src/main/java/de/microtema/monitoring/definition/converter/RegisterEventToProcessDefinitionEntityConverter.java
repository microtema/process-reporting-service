package de.microtema.monitoring.definition.converter;

import de.microtema.model.converter.Converter;
import de.microtema.monitoring.definition.repository.ProcessDefinitionEntity;
import de.microtema.monitoring.event.command.model.RegisterEvent;
import de.microtema.monitoring.utils.BpmnModelInstanceUtils;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.Validate;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.bpmn.instance.BaseElement;
import org.camunda.bpm.model.bpmn.instance.Process;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Optional;

@Log4j2
@Component
public class RegisterEventToProcessDefinitionEntityConverter implements Converter<ProcessDefinitionEntity, RegisterEvent> {

    @Override
    public void update(ProcessDefinitionEntity dest, RegisterEvent orig) {

        var bpmnModelInstance = BpmnModelInstanceUtils.getBpmnModelInstance(orig.getProcessDiagram());

        var process = BpmnModelInstanceUtils.getProcess(bpmnModelInstance, orig.getProcessId());

        var processVersion = BpmnModelInstanceUtils.getVersionTag(bpmnModelInstance, orig.getProcessId(), orig.getProcessVersion());
        dest.setDefinitionMajorVersion(BpmnModelInstanceUtils.getMajorVersion(processVersion));
        dest.setDefinitionVersion(BpmnModelInstanceUtils.getMinorVersion(processVersion));

        dest.setBoundedContext(Optional.ofNullable(process).map(BpmnModelInstanceUtils::getBoundedContext).orElseGet(orig::getBoundedContext));
        dest.setDefinitionKey(Optional.ofNullable(process).map(BaseElement::getId).orElseGet(orig::getProcessId));
        dest.setRetentionTime(orig.getRetentionTime());

        dest.setDefinitionName(orig.getFileName());
        dest.setDefinitionDiagram(orig.getProcessDiagram());
        dest.setDefinitionDeployTime(LocalDateTime.now());
        dest.setLastHeartBeat(LocalDateTime.now());

        update(dest, orig, bpmnModelInstance);
    }

    private void update(ProcessDefinitionEntity dest, RegisterEvent orig, BpmnModelInstance meta) {

        Process process = meta.getModelElementById(orig.getProcessId());

        Validate.notNull(process, "Unable to get process by id: " + orig.getProcessId() + " : " + orig.getFileName());

        dest.setDefinitionDisplayName(process.getName());

        dest.setDefinitionDescription(BpmnModelInstanceUtils.getDescription(process));
    }
}
