package de.microtema.monitoring.event.command.model;

import java.time.LocalDateTime;

public class HeartBeatEvent {

    /**
     * Camunda process id
     */
    private String processId;

    /**
     * Camunda process version 1.0
     */
    private String processVersion;

    /**
     * Heart beat timestamp
     */
    private LocalDateTime eventTime;

    public String getProcessId() {
        return processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }

    public String getProcessVersion() {
        return processVersion;
    }

    public void setProcessVersion(String processVersion) {
        this.processVersion = processVersion;
    }

    public LocalDateTime getEventTime() {
        return eventTime;
    }

    public void setEventTime(LocalDateTime eventTime) {
        this.eventTime = eventTime;
    }
}
