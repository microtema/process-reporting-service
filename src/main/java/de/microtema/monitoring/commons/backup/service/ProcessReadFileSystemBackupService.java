package de.microtema.monitoring.commons.backup.service;

import de.microtema.monitoring.commons.backup.model.BackupType;
import de.microtema.monitoring.definition.model.ProcessDefinition;
import de.microtema.monitoring.definition.service.ProcessDefinitionService;
import de.microtema.monitoring.event.backup.converter.StringToProcessFileSystemConverter;
import de.microtema.monitoring.event.backup.model.ProcessFileSystem;
import de.microtema.monitoring.timerange.model.TimeRangeType;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.io.File;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RequiredArgsConstructor
public abstract class ProcessReadFileSystemBackupService {

    private final FileSystemService fileSystemService;
    private final ProcessDefinitionService processDefinitionService;
    private final StringToProcessFileSystemConverter processFileSystemConverter;

    public Page<ProcessFileSystem> getBackups(int page, int size, String query, String definitionKey, TimeRangeType timeRange, LocalDateTime startDate, LocalDateTime endDate) {

        if (!fileSystemService.existsBackupFolder()) {
            return new PageImpl<>(Collections.emptyList());
        }

        var backupFolders = getBackupFolders(definitionKey);
        var files = getFilteredFiles(backupFolders, query, timeRange, startDate, endDate);

        return getBackups(page, size, files);
    }

    public Page<ProcessFileSystem> getBackups(int page, int size, List<ProcessFileSystem> files) {

        if (files.isEmpty()) {
            return new PageImpl<>(Collections.emptyList());
        }

        var totalElements = files.size();

        var startOffset = Math.max(page * size, 0);
        var endOffset = Math.min(startOffset + size, totalElements);

        var subList = files.subList(startOffset, endOffset);

        subList.forEach(it -> it.setInstanceName(processDefinitionService.getProcessDefinitionByDefinitionKey(it.getDefinitionKey()).getDisplayName()));

        return new PageImpl<>(subList, Pageable.ofSize(size), totalElements);
    }

    private List<ProcessFileSystem> getFilteredFiles(List<File> backupFolders, String query, TimeRangeType timeRange, LocalDateTime startDate, LocalDateTime endDate) {

        var list = new ArrayList<ProcessFileSystem>();

        for (var backupFolder : backupFolders) {

            var files = backupFolder.listFiles();

            if (Objects.isNull(files)) {
                continue;
            }

            for (var file : files) {

                var fileSystem = processFileSystemConverter.convert(file.getName(), backupFolder.getPath());

                if (fileFilter(fileSystem, query, timeRange, startDate, endDate)) {
                    list.add(fileSystem);
                }
            }
        }

        return list;
    }

    private List<File> getBackupFolders(String definitionKey) {

        var backupType = getBackupType();
        var backupFolder = new File(fileSystemService.getBackupFolder(), backupType.name());

        if (StringUtils.isNotEmpty(definitionKey)) {
            return List.of(new File(backupFolder, definitionKey));
        }

        var list = backupFolder.list((dir, fileName) -> !fileName.startsWith("."));

        if (Objects.isNull(list)) {
            return List.of();
        }

        return Stream.of(list).map(it -> new File(backupFolder, it)).sorted().collect(Collectors.toList());
    }

    public abstract BackupType getBackupType();

    private String findProcessName(String definitionKey) {

        return Optional.ofNullable(processDefinitionService.getProcessDefinitionByDefinitionKey(definitionKey)).map(ProcessDefinition::getDisplayName).orElse(definitionKey);
    }

    private boolean fileFilter(File parentFolder, String fileName, String query, TimeRangeType timeRange, LocalDateTime startDate, LocalDateTime endDate) {

        var instanceFileSystem = processFileSystemConverter.convert(fileName, parentFolder.getPath());

        if (!timeRange.between(instanceFileSystem.getCreationTime(), startDate, endDate)) {
            return false;
        }

        return fileFilter(instanceFileSystem, query);
    }

    private boolean fileFilter(ProcessFileSystem instanceFileSystem, String query, TimeRangeType timeRange, LocalDateTime startDate, LocalDateTime endDate) {

        if (!timeRange.between(instanceFileSystem.getCreationTime(), startDate, endDate)) {
            return false;
        }

        return fileFilter(instanceFileSystem, query);
    }

    private boolean fileFilter(ProcessFileSystem fileSystem, String query) {

        if (StringUtils.isEmpty(query)) {
            return true;
        }

        var path = fileSystem.getPath();
        var processName = findProcessName(fileSystem.getDefinitionKey());

        return StringUtils.containsIgnoreCase(processName, query) || StringUtils.containsIgnoreCase(path, query);
    }
}
