package de.microtema.monitoring.definition.converter;

import de.microtema.model.converter.MetaConverter;
import de.microtema.monitoring.definition.model.ProcessDefinition;
import de.microtema.monitoring.definition.model.ProcessDefinitionStatus;
import de.microtema.monitoring.definition.repository.ProcessDefinitionEntity;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class ProcessDefinitionEntityToProcessDefinitionConverter implements MetaConverter<ProcessDefinition, ProcessDefinitionEntity, Function<ProcessDefinitionEntity, ProcessDefinitionStatus>> {

    @Override
    public void update(ProcessDefinition dest, ProcessDefinitionEntity orig) {

        update(dest, orig, (it) -> null);
    }

    @Override
    public void update(ProcessDefinition dest, ProcessDefinitionEntity orig, Function<ProcessDefinitionEntity, ProcessDefinitionStatus> meta) {

        dest.setDefinitionKey(orig.getDefinitionKey());
        dest.setBoundedContext(orig.getBoundedContext());
        dest.setName(orig.getDefinitionName());
        dest.setDisplayName(orig.getDefinitionDisplayName());
        dest.setDescription(orig.getDefinitionDescription());

        dest.setDeployTime(orig.getDefinitionDeployTime());
        dest.setMajorVersion(orig.getDefinitionMajorVersion());
        dest.setDefinitionVersion(orig.getDefinitionVersion());

        dest.setDiagram(orig.getDefinitionDiagram());
        dest.setId(orig.getUuid());

        dest.setDefinitionStatus(meta.apply(orig));

        dest.setLastHeartBeat(orig.getLastHeartBeat());
        dest.setRetentionTime(orig.getRetentionTime());
    }
}
