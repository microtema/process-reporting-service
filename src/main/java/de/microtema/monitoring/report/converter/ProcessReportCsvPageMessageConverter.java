package de.microtema.monitoring.report.converter;

import de.microtema.monitoring.csv.converter.AbstractCsvPageMessageConverter;
import de.microtema.monitoring.report.model.ProcessReport;
import de.microtema.monitoring.report.model.ProcessReportPage;
import org.springframework.stereotype.Component;

@Component
public class ProcessReportCsvPageMessageConverter extends AbstractCsvPageMessageConverter<ProcessReportPage, ProcessReport> {

}
