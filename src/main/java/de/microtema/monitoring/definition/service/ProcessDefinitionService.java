package de.microtema.monitoring.definition.service;

import de.microtema.monitoring.commons.converter.PageRequestDTOToPageRequestConverter;
import de.microtema.monitoring.commons.models.PageRequestDTO;
import de.microtema.monitoring.definition.converter.HeartBeatEventToProcessDefinitionEntityConverter;
import de.microtema.monitoring.definition.converter.ProcessDefinitionEntityToProcessDefinitionConverter;
import de.microtema.monitoring.definition.converter.ProcessDefinitionSpecificationToSpecificationConverter;
import de.microtema.monitoring.definition.converter.RegisterEventToProcessDefinitionEntityConverter;
import de.microtema.monitoring.definition.model.ProcessDefinition;
import de.microtema.monitoring.definition.model.ProcessDefinitionSpecification;
import de.microtema.monitoring.definition.repository.ProcessDefinitionEntity;
import de.microtema.monitoring.definition.repository.ProcessDefinitionRepository;
import de.microtema.monitoring.event.command.model.HeartBeatEvent;
import de.microtema.monitoring.event.command.model.RegisterEvent;
import de.microtema.monitoring.process.repository.ProcessInstanceRepository;
import de.microtema.monitoring.timerange.model.TimeRangeType;
import de.microtema.monitoring.utils.BpmnModelInstanceUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.interceptor.SimpleKey;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Log4j2
@Service
@RequiredArgsConstructor
@CacheConfig(cacheNames = {"definition"})
public class ProcessDefinitionService {

    @Value("${max-retention-days:7}")
    private int maxRetentionDays = 7;

    private final CacheManager cacheManager;
    private final ProcessDefinitionRepository repository;
    private final ProcessInstanceRepository instanceRepository;

    private final RegisterEventToProcessDefinitionEntityConverter processDefinitionEntityConverter;
    private final HeartBeatEventToProcessDefinitionEntityConverter heartBeatEventToProcessDefinitionEntityConverter;
    private final ProcessDefinitionEntityToProcessDefinitionConverter entityToProcessDefinitionConverter;
    private final ProcessDefinitionSpecificationToSpecificationConverter specificationConverter;

    private final PageRequestDTOToPageRequestConverter pageRequestConverter;


    public ProcessDefinition registerProcessDefinition(RegisterEvent registerEvent) {

        var definitionEntity = processDefinitionEntityConverter.convert(registerEvent);

        var recent = repository.findOne(definitionEntity);

        if (recent.isPresent()) {

            updateProcessDefinition(recent.get(), definitionEntity);

            evictProcessDefinitionCache(definitionEntity);

        } else {

            definitionEntity = repository.save(definitionEntity);

            log.info("Definition {} within ttl [{}] day(s) successfully created", definitionEntity.getDefinitionName(), definitionEntity.getRetentionTime());
        }

        return entityToProcessDefinitionConverter.convert(definitionEntity);
    }

    public ProcessDefinition saveLastHeartBeatProcessDefinition(HeartBeatEvent heartBeatEvent) {

        var definitionEntity = heartBeatEventToProcessDefinitionEntityConverter.convert(heartBeatEvent);

        var recent = repository.findOne(definitionEntity);

        return recent
                .map(it -> updateHeartBeatProcessDefinition(it, definitionEntity))
                .map(repository::save)
                .map(entityToProcessDefinitionConverter::convert)
                .orElse(null);
    }

    @Transactional
    @Cacheable
    public List<ProcessDefinition> getProcessDefinitions() {

        return entityToProcessDefinitionConverter.convertList(repository.findAll());
    }

    public Page<ProcessDefinition> getProcessDefinitions(int page, int size, String properties, String query, TimeRangeType timeRange, LocalDateTime startDate, LocalDateTime endDate) {

        var processDefinitionSpecification = ProcessDefinitionSpecification.builder()
                .query(query)
                .timeRange(timeRange)
                .startDate(startDate)
                .endDate(endDate)
                .groupBy("definitionKey")
                .build();

        var pageRequestDTO = PageRequestDTO.of(page, size, properties);

        var specification = specificationConverter.convert(processDefinitionSpecification);
        var pageable = pageRequestConverter.convert(pageRequestDTO);

        var entityPage = repository.findAll(specification, pageable);

        return entityPage.map(it -> entityToProcessDefinitionConverter.convert(it, e -> instanceRepository.getProcessDefinitionStatus(e.getDefinitionKey(), timeRange, startDate, endDate)));
    }

    public ProcessDefinition getProcessDefinition(String id, TimeRangeType timeRange, LocalDateTime startDate, LocalDateTime endDate) {

        var entity = repository.findById(id).orElse(null);

        return entityToProcessDefinitionConverter.convert(entity, it -> instanceRepository.getProcessDefinitionStatus(it.getDefinitionKey(), timeRange, startDate, endDate));
    }

    @Cacheable
    public ProcessDefinition getProcessDefinitionByDefinitionKey(String definitionKey) {

        var processDefinition = repository.findByDefinitionKey(definitionKey).map(entityToProcessDefinitionConverter::convert);

        processDefinition.ifPresent(it -> it.setBpmnModelInstance(BpmnModelInstanceUtils.getBpmnModelInstance(it.getDiagram())));

        return processDefinition.orElse(null);
    }

    @Cacheable
    public ProcessDefinition getProcessDefinitionByDefinitionKeyAndMajorVersion(String definitionKey, Integer majorVersion) {

        var processDefinition = repository.findOne(definitionKey, majorVersion).map(entityToProcessDefinitionConverter::convert);

        processDefinition.ifPresent(it -> it.setBpmnModelInstance(BpmnModelInstanceUtils.getBpmnModelInstance(it.getDiagram())));

        return processDefinition.orElse(null);
    }

    private ProcessDefinitionEntity updateHeartBeatProcessDefinition(ProcessDefinitionEntity recent, ProcessDefinitionEntity definitionEntity) {

        recent.setLastHeartBeat(definitionEntity.getLastHeartBeat());

        return recent;
    }

    private void updateProcessDefinition(ProcessDefinitionEntity recent, ProcessDefinitionEntity definitionEntity) {

        recent.setDefinitionDiagram(definitionEntity.getDefinitionDiagram());

        recent.setDefinitionDisplayName(definitionEntity.getDefinitionDisplayName());
        recent.setDefinitionDescription(definitionEntity.getDefinitionDescription());
        recent.setBoundedContext(definitionEntity.getBoundedContext());
        recent.setDefinitionName(definitionEntity.getDefinitionName());
        recent.setRetentionTime(definitionEntity.getRetentionTime());

        recent.setDefinitionDeployTime(LocalDateTime.now());
        recent.setLastHeartBeat(LocalDateTime.now());

        repository.saveAndFlush(recent);

        log.info("Definition {} within ttl [{}] day(s) successfully updated", definitionEntity.getDefinitionName(), definitionEntity.getRetentionTime());
    }

    private void evictProcessDefinitionCache(ProcessDefinitionEntity definitionEntity) {

        var cache = cacheManager.getCache("definition");

        var definitionKey = definitionEntity.getDefinitionKey();
        var majorVersion = definitionEntity.getDefinitionMajorVersion();

        Optional.ofNullable(cache).ifPresent(it -> {

            it.evictIfPresent(definitionKey);

            var simpleKey = new SimpleKey(definitionKey, majorVersion);

            it.evictIfPresent(simpleKey);

            log.info("Evict Process Definition cache by definitionKey {} and major version {}", definitionKey, majorVersion);
        });
    }
}
