package de.microtema.monitoring.definition.model;

import de.microtema.model.builder.ModelBuilder;
import de.microtema.monitoring.event.command.model.RegisterEvent;
import lombok.SneakyThrows;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;


@Component
public class RegisterEventModelBuilder implements ModelBuilder<RegisterEvent> {

    @Override
    public RegisterEvent min() {

        var min = new RegisterEvent();

        min.setProcessVersion("1.0");
        min.setProcessId("Process_1rm6rmw");

        min.setProcessDiagram(getBpmn());

        min.setFileName("example.bpmn");

        return min;
    }

    @SneakyThrows
    private String getBpmn() {

        var inputStream = RegisterEventModelBuilder.class.getResourceAsStream("/bpmn/process-workflow.bpmnx");

        return IOUtils.toString(inputStream, StandardCharsets.UTF_8);
    }
}
