package de.microtema.monitoring.commons.converter;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;
import java.util.Objects;

@Converter(autoApply = true)
public class BooleanConverter implements AttributeConverter<Boolean, Character> {

    @Override
    public Character convertToDatabaseColumn(Boolean attribute) {
        return attribute == Boolean.TRUE ? '1' : '0';
    }

    @Override
    public Boolean convertToEntityAttribute(Character dbData) {
        return Objects.equals(dbData, '1');
    }
}
