package de.microtema.monitoring.event.backup.converter;

import de.microtema.model.converter.MetaConverter;
import de.microtema.monitoring.event.backup.model.ProcessFileSystem;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;

import java.io.File;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

@Component
public class StringToProcessFileSystemConverter implements MetaConverter<ProcessFileSystem, String, String> {

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy_MM_dd_HH_mm");

    @SneakyThrows
    @Override
    public void update(ProcessFileSystem dest, String orig) {

        File file = new File(orig);

        String meta = file.getParentFile().getName();

        update(dest, orig, meta);
    }

    @SneakyThrows
    @Override
    public void update(ProcessFileSystem dest, String orig, String meta) {

        String[] parts = orig.split("__");

        Date datePart = DATE_FORMAT.parse(parts[0]);

        parts = parts[1].split("_");
        int pagePart = Integer.parseInt(parts[0]);

        parts = parts[1].split("\\.");
        int elementCountPart = Integer.parseInt(parts[0]);

        String definitionKey = new File(meta).getName();

        dest.setPath(meta + "/" + orig);
        dest.setDefinitionKey(definitionKey);
        dest.setInstanceName(definitionKey);

        dest.setPage(pagePart);
        dest.setElementCount(elementCountPart);
        dest.setCreationTime(LocalDateTime.ofInstant(datePart.toInstant(), ZoneId.systemDefault()));
    }
}
