package de.microtema.monitoring.log.model;

import de.microtema.monitoring.timerange.model.TimeRangeType;
import lombok.Builder;
import lombok.Value;

import java.time.LocalDateTime;

@Builder
@Value
public class LogSpecification {

    String query;

    String runtimeName;

    LogLevel logLevel;

    TimeRangeType timeRange;

    LocalDateTime startDate;

    LocalDateTime endDate;
}
