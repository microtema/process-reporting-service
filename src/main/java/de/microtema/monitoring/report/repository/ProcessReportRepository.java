package de.microtema.monitoring.report.repository;

import de.microtema.model.converter.util.CollectionUtil;
import de.microtema.monitoring.commons.repository.BaseEntityRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProcessReportRepository extends BaseEntityRepository<ProcessReportEntity> {

    default List<ProcessReportEntity> findAllBy(String businessKey, int retryCount) {

        return findAllByProcessBusinessKeyAndRetryCountOrderByReportStartTime(businessKey, retryCount);
    }

    List<ProcessReportEntity> findAllByProcessBusinessKeyAndRetryCountOrderByReportStartTime(String businessKey, int retryCount);

    default Optional<ProcessReportEntity> findOne(ProcessReportEntity queryEntity) {

        var definitionKey = queryEntity.getDefinitionKey();
        var reportId = queryEntity.getReportId();
        var businessKey = queryEntity.getProcessBusinessKey();
        var instanceId = queryEntity.getInstanceId();
        var multipleInstanceIndex = queryEntity.getMultipleInstanceIndex();
        var retryCount = queryEntity.getRetryCount();

        var entries = findAllByDefinitionKeyAndReportIdAndProcessBusinessKeyAndInstanceIdAndMultipleInstanceIndexAndRetryCount(definitionKey, reportId, businessKey, instanceId, multipleInstanceIndex, retryCount);

        return Optional.ofNullable(CollectionUtil.last(entries));
    }

    List<ProcessReportEntity> findAllByDefinitionKeyAndReportIdAndProcessBusinessKeyAndInstanceIdAndMultipleInstanceIndexAndRetryCount(String definitionKey,
                                                                                                                                                    String reportId,
                                                                                                                                                    String businessKey,
                                                                                                                                                    String instanceId,
                                                                                                                                                    String multipleInstanceIndex,
                                                                                                                                                    int retryCount);
}
