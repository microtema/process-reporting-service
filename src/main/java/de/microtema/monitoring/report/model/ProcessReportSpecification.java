package de.microtema.monitoring.report.model;

import de.microtema.monitoring.commons.models.ProcessStatus;
import de.microtema.monitoring.timerange.model.TimeRangeType;
import lombok.Builder;
import lombok.Value;

import java.time.LocalDateTime;

@Builder
@Value
public class ProcessReportSpecification {

     String query;

     String definitionKey;

     ProcessStatus reportStatus;

     TimeRangeType timeRange;

     LocalDateTime startDate;

     LocalDateTime endDate;
}
