package de.microtema.monitoring.message.converter;

import de.microtema.model.converter.Converter;
import de.microtema.monitoring.message.model.ProcessMessage;
import de.microtema.monitoring.message.repository.ProcessMessageEntity;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class ProcessMessageEntityToProcessMessageConverter implements Converter<ProcessMessage, ProcessMessageEntity> {

    @Override
    public void update(ProcessMessage dest, ProcessMessageEntity orig) {
        Converter.super.update(dest, orig);

        dest.setErrorMessage(Optional.ofNullable(orig.getErrorMessage()).orElseGet(orig::getErrorMessageHash));
    }
}
