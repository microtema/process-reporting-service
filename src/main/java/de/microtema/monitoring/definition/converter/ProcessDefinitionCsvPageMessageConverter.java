package de.microtema.monitoring.definition.converter;

import de.microtema.monitoring.csv.converter.AbstractCsvPageMessageConverter;
import de.microtema.monitoring.definition.model.ProcessDefinition;
import de.microtema.monitoring.definition.model.ProcessDefinitionPage;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ProcessDefinitionCsvPageMessageConverter extends AbstractCsvPageMessageConverter<ProcessDefinitionPage, ProcessDefinition> {

    @Override
    public List<ProcessDefinition> getContent(ProcessDefinitionPage page) {

        var content = page.getContent();

        content.forEach(it -> it.setDiagram(null));

        return content;
    }
}
