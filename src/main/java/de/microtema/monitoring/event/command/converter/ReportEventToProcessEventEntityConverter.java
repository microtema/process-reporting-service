package de.microtema.monitoring.event.command.converter;

import de.microtema.model.converter.MetaConverter;
import de.microtema.monitoring.commons.models.ProcessStatus;
import de.microtema.monitoring.definition.model.ProcessDefinition;
import de.microtema.monitoring.event.command.model.ReportEvent;
import de.microtema.monitoring.event.command.repository.ProcessEventEntity;
import de.microtema.monitoring.utils.CommonUtils;
import org.springframework.stereotype.Component;

import java.util.Optional;

import static de.microtema.monitoring.utils.CommonUtils.getReportName;
import static de.microtema.monitoring.utils.CommonUtils.getReportStatus;

@Component
public class ReportEventToProcessEventEntityConverter implements MetaConverter<ProcessEventEntity, ReportEvent, ProcessDefinition> {

    @Override
    public ProcessEventEntity convert(ReportEvent orig) {

        return convert(orig, null);
    }

    @Override
    public void update(ProcessEventEntity dest, ReportEvent orig, ProcessDefinition meta) {

        dest.setProcessBusinessKey(orig.getTransactionId());
        dest.setInstanceId(orig.getExecutionId());

        dest.setReportId(orig.getElementId());
        dest.setReportName(getReportName(orig, meta));
        dest.setInstanceName(Optional.ofNullable(meta).map(ProcessDefinition::getDisplayName).orElseGet(orig::getProcessId));

        dest.setDefinitionKey(orig.getProcessId());
        dest.setMultipleInstanceIndex(Optional.ofNullable(orig.getMultipleInstanceIndex()).orElse("0.0"));

        dest.setErrorMessage(orig.getErrorMessage());
        dest.setErrorMessageShort(CommonUtils.substring(orig.getErrorMessage(), 256, true));

        dest.setStartEvent(orig.isStartEvent());
        dest.setEndEvent(orig.isEndEvent());

        dest.setRetryCount(orig.getRetryCount());

        dest.setReportStartTime(orig.getEventTime());
        dest.setReportEndTime(orig.getEventTime());

        // Meta infos
        dest.setReferenceId(CommonUtils.substring(orig.getReferenceId(), 256, true));
        dest.setReferenceType(orig.getReferenceType());
        dest.setStarterId(orig.getStartedBy());

        var reportStatus = ProcessStatus.valueOf(orig.getStatus().name());

        if (reportStatus.is(ProcessStatus.STARTED, ProcessStatus.QUEUED)) {

            dest.setReportStatus(reportStatus);
            dest.setReportStartTime(orig.getEventTime());
            dest.setInputPayload(orig.getPayload());

            if (reportStatus.is(ProcessStatus.QUEUED)) {
                dest.setReportEndTime(orig.getEventTime());
                dest.setOutputPayload(orig.getPayload());
            }

        } else {

            dest.setReportStatus(getReportStatus(orig, meta));
            dest.setReportEndTime(orig.getEventTime());
            dest.setOutputPayload(orig.getPayload());
        }
    }
}
