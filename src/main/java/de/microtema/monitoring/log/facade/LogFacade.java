package de.microtema.monitoring.log.facade;

import de.microtema.monitoring.log.model.*;
import de.microtema.monitoring.log.service.LogReadService;
import de.microtema.monitoring.log.service.LogWriteService;
import de.microtema.monitoring.timerange.model.TimeRangeType;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class LogFacade {

    private final LogWriteService logWriteService;
    private final LogReadService logReadService;

    public String saveLogEvent(LogReportEvent event) {

        return logWriteService.saveLogEvent(event);
    }

    public LogData getLogData(String uuid) {

        return logReadService.getLogData(uuid);
    }

    public Page<LogData> getLogDataPage(int page, int size, String properties, String query, String runtimeName, LogLevel logLevel, TimeRangeType timeRange, LocalDateTime startDate, LocalDateTime endDate) {

        return logReadService.getLogDataPage(page, size, properties, query, runtimeName, logLevel, timeRange, startDate, endDate);
    }

    public List<LogLevelDTO> getLogLevelList() {

        return logReadService.getLogLevelList();
    }

    public List<RuntimeData> getRunTimes() {

        return logReadService.getRunTimes();
    }
}
