## Requirements

![Event Reporting Workflow](Resources/package-structure.png)

* Maven dependency
  ```
    <dependency>
        <groupId>de.microtema</groupId>
        <artifactId>process-reporting-starter</artifactId>
    </dependency>
  ```
* @BpmnElement
  ```
    @BpmnElement(id = "Event_1anjljr", keyExpression = "{{PO_NUMBER}}")
    public PurchaseOrder execute(PurchaseOrderEvent event) {

        return ...;
    }

    ----

    @BpmnElement(id = "Activity_194xhhj")
    public ProcurementOrder execute(PurchaseOrder data) {

        return ...;
    }

    ---

    @BpmnElement(id = "Event_0byexdn", endEvent = true)
    public ProcurementOrder execute(ProcurementOrder procurementOrder, boolean purchaseOrderExists) {

        return ...;
    }
  ```
* application.yaml
  ```
  reporting:
    process-id: Process_0e2hw2v
    server: http://localhost:9090/reporting-service/rest
    process-version: 1.0
  ```
* BPMN Template.bpmn
  * ![Event Reporting Workflow](Resources/process-flow.png)
  * ![Event Reporting Workflow](Resources/process-flow-2.png)

## Oracle XE

### docker cmd

```
docker run -d -p 49161:1521 -p 8080:8080 wnameless/oracle-xe-11g-r2:18.04-apex
```

```
docker run --name postgresql -e POSTGRESQL_USERNAME=postgres -e POSTGRESQL_PASSWORD=postgres -e POSTGRESQL_DATABASE=postgres bitnami/postgresql:latest
```

### find running process

```
netstat -vanp tcp | grep 49161

tcp6       0      0  fe80::aede:48ff:.49161 fe80::aede:48ff:.49196 ESTABLISHED 131083 131376    307      0 0x0182 0x00000000

sudo kill -9 307
```

* hostname: localhost
* port: 49161
* sid: xe
* username: system
* password: oracle
* url: jdbc:oracle:thin:@tcp://localhost:49161/xe
* Link: https://github.com/wnameless/docker-oracle-xe-11g

## Questions

| Question | Answer | From | Date |
| -------- | ------ | ---- | ---- |
| What kind of Authentication is supported?  | none | @Mario |  |
