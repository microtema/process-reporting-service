package de.microtema.monitoring.definition.controller;

import de.microtema.monitoring.definition.facade.ProcessDefinitionFacade;
import de.microtema.monitoring.definition.model.ProcessDefinition;
import de.microtema.monitoring.definition.model.ProcessDefinitionPage;
import de.microtema.monitoring.event.command.model.HeartBeatEvent;
import de.microtema.monitoring.event.command.model.RegisterEvent;
import de.microtema.monitoring.timerange.model.TimeRangeType;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@Log4j2
@RestController
@RequiredArgsConstructor
@RequestMapping("/rest/api/definition")
public class ProcessDefinitionController {

    private final ProcessDefinitionFacade facade;

    /**
     * @param event may not be null
     * @return ResponseEntity
     */
    @PostMapping
    public ResponseEntity<ProcessDefinition> registerProcessDefinition(@RequestBody RegisterEvent event) {

        log.info("The {} event {} has been accepted for processing.", "RegisterEvent", event.getFileName());

        return ResponseEntity.ok(facade.registerProcessDefinition(event));
    }

    /**
     * @param event may not be null
     * @return ResponseEntity
     */
    @PostMapping(value = "/heart-beat")
    public ResponseEntity<ProcessDefinition> saveLastHeartBeatProcessDefinition(@RequestBody HeartBeatEvent event) {

        log.debug("The {} event {} has been accepted for processing.", "HeartBeatEvent", event.getProcessId());

        return ResponseEntity.ok(facade.saveLastHeartBeatProcessDefinition(event));
    }

    @GetMapping
    public ResponseEntity<ProcessDefinitionPage> getProcessDefinitions(
            @RequestParam(name = "page", required = false, defaultValue = "0") int page,
            @RequestParam(name = "size", required = false, defaultValue = "25") int size,
            @RequestParam(name = "properties", required = false, defaultValue = "!definitionDisplayName") String properties,
            @RequestParam(name = "query", required = false) String query,
            @RequestParam(name = "timeRange", required = false, defaultValue = "ALL") TimeRangeType timeRange,
            @RequestParam(name = "startDate", required = false) LocalDateTime startDate,
            @RequestParam(name = "endDate", required = false) LocalDateTime endDate
    ) {

        return ResponseEntity.ok(new ProcessDefinitionPage(facade.getProcessDefinitions(page, size, properties, query, timeRange, startDate, endDate)));
    }

    @GetMapping(value = "/processes")
    public ResponseEntity<List<ProcessDefinition>> getProcessDefinitions() {

        return ResponseEntity.ok(facade.getProcessDefinitions());
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<ProcessDefinition> getProcessDefinition(
            @PathVariable String id,
            @RequestParam(name = "timeRange", required = false, defaultValue = "ALL") TimeRangeType timeRange,
            @RequestParam(name = "startDate", required = false) LocalDateTime startDate,
            @RequestParam(name = "endDate", required = false) LocalDateTime endDate
            ) {

        return ResponseEntity.ok(facade.getProcessDefinition(id, timeRange, startDate, endDate));
    }

    @GetMapping(value = "/key/{key}")
    public ResponseEntity<ProcessDefinition> getProcessDefinitionByKey(@PathVariable String key) {

        return ResponseEntity.ok(facade.getProcessDefinitionByKey(key));
    }
}
