package de.microtema.monitoring.report.command.repository;

import de.microtema.model.builder.annotation.Model;
import de.microtema.model.builder.enums.ModelType;
import de.microtema.model.builder.util.FieldInjectionUtil;
import de.microtema.monitoring.Application;
import de.microtema.monitoring.commons.models.ProcessStatus;
import de.microtema.monitoring.event.command.repository.ProcessEventEntity;
import de.microtema.monitoring.event.command.repository.ProcessEventRepository;
import jakarta.inject.Inject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {Application.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ProcessReportRepositoryIT {

    @Inject
    ProcessEventRepository sut;

    @Model(type = ModelType.MAX)
    ProcessEventEntity model;

    @BeforeEach
    void setUp() {
        FieldInjectionUtil.injectFields(this);

        model.setUuid(null);
        model.setRetryCount(0);
        model.setReportStatus(ProcessStatus.STARTED);

        sut.save(model);
    }

    @Test
    void findOne() {

        var answer = sut.findById(model.getUuid());

        assertTrue(answer.isPresent());
        assertEquals(model.getProcessBusinessKey(), answer.get().getProcessBusinessKey());
    }
}
