package de.microtema.monitoring.log.converter;

import de.microtema.model.converter.Converter;
import de.microtema.monitoring.log.model.LogLevel;
import de.microtema.monitoring.log.model.LogLevelDTO;
import org.springframework.stereotype.Component;

@Component
public class LogLevelToLogLevelDTOConverter implements Converter<LogLevelDTO, LogLevel> {

    @Override
    public void update(LogLevelDTO dest, LogLevel orig) {

        dest.setValue(orig.name());
        dest.setDisplayName(orig.getDisplayName());
    }
}
