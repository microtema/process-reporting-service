package de.microtema.monitoring.utils;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.MustacheFactory;
import lombok.experimental.UtilityClass;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.Map;

@UtilityClass
public class TemplateUtil {

    private final static MustacheFactory mustacheFactory = new DefaultMustacheFactory();

    public static String compileExpression(String template, Map<String, Object> context) {

        var mustache = mustacheFactory.compile(new StringReader(template), template);

        var execute = mustache.execute(new StringWriter(), context);

        return execute.toString();
    }
}
