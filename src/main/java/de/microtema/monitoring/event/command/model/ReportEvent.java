package de.microtema.monitoring.event.command.model;

import java.time.LocalDateTime;
import java.util.Map;

public class ReportEvent {

    /**
     * Camunda Element id
     */
    private String elementId;

    private String transactionId;

    /**
     * Camunda process id
     */
    private String processId;

    /**
     * Unique Execution id
     */
    private String executionId;

    /**
     * Camunda process version 1.0
     */
    private String processVersion;

    /**
     * Error message
     */
    private String errorMessage;

    /**
     * Event status, [STARTED, COMPLETED, ERROR, ...]
     */
    private ReportStatus status;

    /**
     * Event timestamp
     */
    private LocalDateTime eventTime;

    /**
     * Technical meta information of retry the same event
     */
    private int retryCount;

    /**
     * NOTE: if size of payload or error message is longer then supported, we need to chunked in multiple parts
     */
    private int partCount;

    /**
     * Reference id/key of this event
     */
    private String referenceId;

    /**
     * Bounded Context of this event, [user, invoice, billing, ...]
     */
    private String referenceType;

    /**
     * Who trigger the event, [user, technical-user, machine, ...]
     */
    private String startedBy;

    /**
     * Multiple Instance Count for the same activity butt different data
     */
    private String multipleInstanceIndex;

    /**
     * Serialized JSON payload
     */
    private String payload;

    /**
     * Event meta information
     */
    private Map<String, Object> params;

    private boolean startEvent;

    private boolean endEvent;

    public String getElementId() {
        return elementId;
    }

    public void setElementId(String elementId) {
        this.elementId = elementId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getProcessId() {
        return processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }

    public String getExecutionId() {
        return executionId;
    }

    public void setExecutionId(String executionId) {
        this.executionId = executionId;
    }

    public String getProcessVersion() {
        return processVersion;
    }

    public void setProcessVersion(String processVersion) {
        this.processVersion = processVersion;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public ReportStatus getStatus() {
        return status;
    }

    public void setStatus(ReportStatus status) {
        this.status = status;
    }

    public LocalDateTime getEventTime() {
        return eventTime;
    }

    public void setEventTime(LocalDateTime eventTime) {
        this.eventTime = eventTime;
    }

    public int getRetryCount() {
        return retryCount;
    }

    public void setRetryCount(int retryCount) {
        this.retryCount = retryCount;
    }

    public int getPartCount() {
        return partCount;
    }

    public void setPartCount(int partCount) {
        this.partCount = partCount;
    }

    public Map<String, Object> getParams() {
        return params;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public String getReferenceType() {
        return referenceType;
    }

    public void setReferenceType(String referenceType) {
        this.referenceType = referenceType;
    }

    public String getStartedBy() {
        return startedBy;
    }

    public void setStartedBy(String startedBy) {
        this.startedBy = startedBy;
    }

    public String getMultipleInstanceIndex() {
        return multipleInstanceIndex;
    }

    public void setMultipleInstanceIndex(String multipleInstanceIndex) {
        this.multipleInstanceIndex = multipleInstanceIndex;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    public boolean isStartEvent() {
        return startEvent;
    }

    public void setStartEvent(boolean startEvent) {
        this.startEvent = startEvent;
    }

    public boolean isEndEvent() {
        return endEvent;
    }

    public void setEndEvent(boolean endEvent) {
        this.endEvent = endEvent;
    }
}
