package de.microtema.monitoring.commons.models;

import java.util.stream.Stream;

public enum ProcessStatus {

    QUEUED("Pending"),
    STARTED("Started"),
    RESTARTED("Restarted"),
    COMPLETED("Completed"),
    TERMINATED("Terminated"),
    PROCESS_COMPLETED("Process Completed"),
    WARNING("Warning"),
    ERROR("Error"),
    ALL("All Statuses");

    private final String displayName;

    ProcessStatus(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {

        return displayName;
    }

    public boolean is(ProcessStatus... status) {

        return Stream.of(status).anyMatch(it -> it == this);
    }

    public boolean not(ProcessStatus... status) {

        return Stream.of(status).noneMatch(it -> it == this);
    }

    public static ProcessStatus fromOrdinal(int ordinal) {
        return ProcessStatus.values()[ordinal];
    }
}
