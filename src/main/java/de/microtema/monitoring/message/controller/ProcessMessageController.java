package de.microtema.monitoring.message.controller;

import de.microtema.monitoring.commons.models.ProcessStatus;
import de.microtema.monitoring.commons.models.ProcessStatusDTO;
import de.microtema.monitoring.message.facade.ProcessMessageFacade;
import de.microtema.monitoring.message.model.ProcessMessagePage;
import de.microtema.monitoring.timerange.model.TimeRangeType;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/rest/api/message")
public class ProcessMessageController {

    private final ProcessMessageFacade facade;

    @GetMapping
    public ResponseEntity<ProcessMessagePage> getProcessMessages(
            @RequestParam(name = "page", required = false, defaultValue = "0") int page,
            @RequestParam(name = "size", required = false, defaultValue = "25") int size,
            @RequestParam(name = "properties", required = false, defaultValue = "totalCount") String properties,
            @RequestParam(name = "query", required = false) String query,
            @RequestParam(name = "definitionKey", required = false) String definitionKey,
            @RequestParam(name = "status", required = false, defaultValue = "ALL") ProcessStatus processStatus,
            @RequestParam(name = "timeRange", required = false, defaultValue = "ALL") TimeRangeType timeRange,
            @RequestParam(name = "startDate", required = false) LocalDateTime startDate,
            @RequestParam(name = "endDate", required = false) LocalDateTime endDat) {

        return ResponseEntity.ok(new ProcessMessagePage(facade.getProcessMessages(page, size, properties, query, definitionKey, processStatus, timeRange, startDate, endDat)));
    }

    @GetMapping(value = "/status")
    public ResponseEntity<List<ProcessStatusDTO>> getProcessInstanceStatusList() {

        return ResponseEntity.ok(facade.getProcessMessageStatusList());
    }
}
