package de.microtema.monitoring.commons.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

@NoRepositoryBean
public interface BaseEntityRepository<E extends BaseEntity> extends JpaRepository<E, String>, JpaSpecificationExecutor<E>, PagingAndSortingRepository<E, String> {

    default Optional<E> findOne(E entity) {

        return findById(entity.getUuid());
    }

    Page<E> findAll(Specification<E> specification, Pageable pageable);
}
