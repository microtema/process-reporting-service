package de.microtema.monitoring.process.controller;

import de.microtema.model.builder.annotation.Model;
import de.microtema.model.builder.enums.ModelType;
import de.microtema.model.builder.util.FieldInjectionUtil;
import de.microtema.monitoring.Application;
import de.microtema.monitoring.event.command.model.ReportEvent;
import de.microtema.monitoring.event.command.service.ProcessEventWriteService;
import de.microtema.monitoring.process.model.ProcessInstance;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import jakarta.inject.Inject;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {Application.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ProcessInstanceControllerIT {

    @Inject
    ProcessInstanceController sut;

    @Inject
    ProcessEventWriteService processEventWriteService;

    @Inject
    TestRestTemplate restTemplate;

    @Model(type = ModelType.MAX)
    ReportEvent reportEvent;

    String url = "/rest/api/process";

    @BeforeEach
    void setUp() {

        FieldInjectionUtil.injectFields(this);

        reportEvent.setProcessVersion("1.0");

        processEventWriteService.saveProcessReport(reportEvent);
    }

    @Test
    void getProcesses() {

        url += "?page=0&size=10&query=&properties=!instanceStartTime&query=&definitionKey=&status=ALL&timeRange=ALL";

        var response = restTemplate.exchange(url, HttpMethod.GET, null, String.class);

        assertNotNull(response);
        assertSame(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    void getProcessByProcessBusinessKey() {

        url += "/businessKey/" + reportEvent.getTransactionId() + "/" + reportEvent.getRetryCount();

        var response = restTemplate.exchange(url, HttpMethod.GET, null, ProcessInstance.class);

        assertNotNull(response);
        assertSame(HttpStatus.OK, response.getStatusCode());
    }
}
