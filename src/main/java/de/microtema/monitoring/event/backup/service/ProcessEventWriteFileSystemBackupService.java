package de.microtema.monitoring.event.backup.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.microtema.monitoring.commons.backup.model.BackupType;
import de.microtema.monitoring.commons.backup.service.FileSystemService;
import de.microtema.monitoring.commons.backup.service.ProcessWriteFileSystemBackupService;
import de.microtema.monitoring.event.command.repository.ProcessEventEntity;
import de.microtema.monitoring.event.command.repository.ProcessEventRepository;
import org.springframework.stereotype.Service;

@Service
public class ProcessEventWriteFileSystemBackupService extends ProcessWriteFileSystemBackupService<ProcessEventEntity> {

    public ProcessEventWriteFileSystemBackupService(FileSystemService fileSystemService, ObjectMapper objectMapper,
                                                    ProcessEventRepository repository) {
        super(fileSystemService, objectMapper, repository);
    }

    @Override
    public BackupType getBackupType() {
        return BackupType.EVENT;
    }

    @Override
    public String getStartTimeProperty() {

        return "reportStartTime";
    }
}
