package de.microtema.monitoring.report.model;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

public class ProcessReportPage extends PageImpl<ProcessReport> {

    public ProcessReportPage(Page<ProcessReport> page) {
        super(page.getContent(), page.getPageable(), page.getTotalElements());
    }
}
