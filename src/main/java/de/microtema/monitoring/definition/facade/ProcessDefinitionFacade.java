package de.microtema.monitoring.definition.facade;

import de.microtema.monitoring.definition.model.ProcessDefinition;
import de.microtema.monitoring.definition.service.ProcessDefinitionService;
import de.microtema.monitoring.event.command.model.HeartBeatEvent;
import de.microtema.monitoring.event.command.model.RegisterEvent;
import de.microtema.monitoring.timerange.model.TimeRangeType;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Component
@RequiredArgsConstructor
public class ProcessDefinitionFacade {

    private final ProcessDefinitionService service;

    public ProcessDefinition registerProcessDefinition(RegisterEvent registerEvent) {

        return service.registerProcessDefinition(registerEvent);
    }

    public ProcessDefinition saveLastHeartBeatProcessDefinition(HeartBeatEvent heartBeatEvent) {

        return service.saveLastHeartBeatProcessDefinition(heartBeatEvent);
    }

    public Page<ProcessDefinition> getProcessDefinitions(int page, int size, String properties, String query, TimeRangeType timeRange, LocalDateTime startDate, LocalDateTime endDate) {

        return service.getProcessDefinitions(page, size, properties, query, timeRange, startDate, endDate);
    }

    public List<ProcessDefinition> getProcessDefinitions() {

        return service.getProcessDefinitions();
    }

    public ProcessDefinition getProcessDefinition(String id, TimeRangeType timeRange, LocalDateTime startDate, LocalDateTime endDate) {

        return service.getProcessDefinition(id, timeRange, startDate, endDate);
    }

    public ProcessDefinition getProcessDefinitionByKey(String key) {

        return service.getProcessDefinitionByDefinitionKey(key);
    }
}
