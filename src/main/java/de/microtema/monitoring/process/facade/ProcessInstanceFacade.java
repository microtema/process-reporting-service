package de.microtema.monitoring.process.facade;

import de.microtema.monitoring.commons.models.ProcessStatus;
import de.microtema.monitoring.commons.models.ProcessStatusDTO;
import de.microtema.monitoring.commons.service.ProcessStatusService;
import de.microtema.monitoring.process.model.ProcessInstance;
import de.microtema.monitoring.process.service.ProcessInstanceReadService;
import de.microtema.monitoring.timerange.model.TimeRangeType;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDateTime;
import java.util.List;

@Component
@RequiredArgsConstructor
public class ProcessInstanceFacade {

    private final ProcessStatusService processStatusService;
    private final ProcessInstanceReadService processInstanceReadService;

    public Page<ProcessInstance> getProcesses(int page, int size, String properties, String query, String definitionKey, ProcessStatus processStatus, TimeRangeType timeRange, LocalDateTime startDate, LocalDateTime endDate) {

        return processInstanceReadService.getProcesses(page, size, properties, query, definitionKey, processStatus, timeRange, startDate, endDate);
    }

    public ProcessInstance findProcessInstance(String businessKey, int retryCount) {

        return processInstanceReadService.findProcessInstance(businessKey, retryCount);
    }

    public List<ProcessStatusDTO> getProcessInstanceStatusList() {

        return processStatusService.getProcessStatusList();
    }
}
