package de.microtema.monitoring.event.query.controller;

import de.microtema.monitoring.commons.models.ProcessStatus;
import de.microtema.monitoring.commons.models.ProcessStatusDTO;
import de.microtema.monitoring.event.query.facade.ProcessEventViewFacade;
import de.microtema.monitoring.report.model.ProcessReport;
import de.microtema.monitoring.report.model.ProcessReportPage;
import de.microtema.monitoring.timerange.model.TimeRangeType;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@Log4j2
@RestController
@RequiredArgsConstructor
@RequestMapping("/rest/api/event")
public class ProcessEventViewController {

    private final ProcessEventViewFacade facade;

    @GetMapping
    public ResponseEntity<ProcessReportPage> getProcessReports(
            @RequestParam(name = "page", required = false, defaultValue = "0") int page,
            @RequestParam(name = "size", required = false, defaultValue = "25") int size,
            @RequestParam(name = "properties", required = false, defaultValue = "reportStartTime") String properties,
            @RequestParam(name = "query", required = false) String query,
            @RequestParam(name = "definitionKey", required = false) String definitionKey,
            @RequestParam(name = "status", required = false, defaultValue = "ALL") ProcessStatus reportStatus,
            @RequestParam(name = "timeRange", required = false, defaultValue = "ALL") TimeRangeType timeRange,
            @RequestParam(name = "startDate", required = false) LocalDateTime startDate,
            @RequestParam(name = "endDate", required = false) LocalDateTime endDate) {

        return ResponseEntity.ok(new ProcessReportPage(facade.getProcessReports(page, size, properties, query, definitionKey, reportStatus, timeRange, startDate, endDate)));
    }

    @GetMapping("/{definitionKey}/{transactionId}/{retryCount}")
    public ResponseEntity<List<ProcessReport>> getProcessEvents(@PathVariable String definitionKey, @PathVariable String transactionId, @PathVariable int retryCount) {

        return ResponseEntity.ok(facade.getProcessEvents(definitionKey, transactionId, retryCount));
    }

    @GetMapping(value = "/{uuid}")
    public ResponseEntity<ProcessReport> getProcessEvent(@PathVariable String uuid) {

        return ResponseEntity.ok(facade.getProcessReport(uuid));
    }

    @GetMapping(value = "/status")
    public ResponseEntity<List<ProcessStatusDTO>> getReportStatusList() {

        return ResponseEntity.ok(facade.getReportStatusList());
    }
}
