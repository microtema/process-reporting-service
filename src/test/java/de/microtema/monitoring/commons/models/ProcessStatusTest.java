package de.microtema.monitoring.commons.models;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ProcessStatusTest {

    ProcessStatus sut;

    @Test
    void length() {

        var answer = ProcessStatus.values().length;

        assertEquals(9, answer);
    }
}
