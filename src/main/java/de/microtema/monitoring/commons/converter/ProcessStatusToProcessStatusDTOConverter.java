package de.microtema.monitoring.commons.converter;

import de.microtema.model.converter.Converter;
import de.microtema.monitoring.commons.models.ProcessStatus;
import de.microtema.monitoring.commons.models.ProcessStatusDTO;
import org.springframework.stereotype.Component;

@Component
public class ProcessStatusToProcessStatusDTOConverter implements Converter<ProcessStatusDTO, ProcessStatus> {

    @Override
    public void update(ProcessStatusDTO dest, ProcessStatus orig) {

        dest.setValue(orig.name());
        dest.setDisplayName(orig.getDisplayName());
    }
}
