package de.microtema.monitoring.definition.repository;


import de.microtema.monitoring.commons.repository.BaseEntity;
import jakarta.persistence.criteria.CriteriaBuilder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity(name = "ProcessDefinition")
@Table(name = "PROCESS_DEFINITION", uniqueConstraints = {@UniqueConstraint(columnNames = {"DEFINITION_KEY", "DEFINITION_MAJOR_VERSION", "DEFINITION_VERSION",})})
public class ProcessDefinitionEntity extends BaseEntity implements Comparable<ProcessDefinitionEntity> {

    @Column(name = "DEFINITION_KEY")
    private String definitionKey;

    @Column(name = "BOUNDED_CONTEXT")
    private String boundedContext;

    @Column(name = "DEFINITION_NAME")
    private String definitionName;

    @Column(name = "DEFINITION_DISPLAY_NAME")
    private String definitionDisplayName;

    @Column(name = "DEFINITION_DESCRIPTION")
    private String definitionDescription;

    @Column(name = "DEFINITION_VERSION")
    private Integer definitionVersion;

    @Column(name = "DEFINITION_MAJOR_VERSION")
    private Integer definitionMajorVersion;

    @Column(name = "DEFINITION_DEPLOY_TIME")
    private LocalDateTime definitionDeployTime;

    @Column(columnDefinition = "Text", name = "DEFINITION_DIAGRAM")
    private String definitionDiagram;

    @Column(name = "LAST_HEART_BEAT")
    private LocalDateTime lastHeartBeat;

    @Column(name = "RETENTION_TIME")
    private Integer retentionTime;

    @Override
    public int compareTo(ProcessDefinitionEntity o) {

        return o.getDefinitionDeployTime().compareTo(definitionDeployTime);
    }

    public String getDefinitionKey() {
        return definitionKey;
    }

    public void setDefinitionKey(String definitionKey) {
        this.definitionKey = definitionKey;
    }

    public String getBoundedContext() {
        return boundedContext;
    }

    public void setBoundedContext(String boundedContext) {
        this.boundedContext = boundedContext;
    }

    public String getDefinitionName() {
        return definitionName;
    }

    public void setDefinitionName(String definitionName) {
        this.definitionName = definitionName;
    }

    public String getDefinitionDisplayName() {
        return definitionDisplayName;
    }

    public void setDefinitionDisplayName(String definitionDisplayName) {
        this.definitionDisplayName = definitionDisplayName;
    }

    public String getDefinitionDescription() {
        return definitionDescription;
    }

    public void setDefinitionDescription(String definitionDescription) {
        this.definitionDescription = definitionDescription;
    }

    public Integer getDefinitionVersion() {
        return definitionVersion;
    }

    public void setDefinitionVersion(Integer definitionVersion) {
        this.definitionVersion = definitionVersion;
    }

    public Integer getDefinitionMajorVersion() {
        return definitionMajorVersion;
    }

    public void setDefinitionMajorVersion(Integer definitionMajorVersion) {
        this.definitionMajorVersion = definitionMajorVersion;
    }

    public LocalDateTime getDefinitionDeployTime() {
        return definitionDeployTime;
    }

    public void setDefinitionDeployTime(LocalDateTime definitionDeployTime) {
        this.definitionDeployTime = definitionDeployTime;
    }

    public String getDefinitionDiagram() {
        return definitionDiagram;
    }

    public void setDefinitionDiagram(String definitionDiagram) {
        this.definitionDiagram = definitionDiagram;
    }

    public LocalDateTime getLastHeartBeat() {
        return lastHeartBeat;
    }

    public void setLastHeartBeat(LocalDateTime lastHeartBeat) {
        this.lastHeartBeat = lastHeartBeat;
    }
}
