package de.microtema.monitoring.process.repository;

import de.microtema.model.converter.util.CollectionUtil;
import de.microtema.monitoring.commons.models.ProcessStatus;
import de.microtema.monitoring.commons.repository.BaseEntityRepository;
import de.microtema.monitoring.definition.model.ProcessDefinitionStatus;
import de.microtema.monitoring.process.model.ProcessStatusProjection;
import de.microtema.monitoring.timerange.model.TimeRangeType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface ProcessInstanceRepository extends BaseEntityRepository<ProcessInstanceEntity> {

    default Optional<ProcessInstanceEntity> findOne(ProcessInstanceEntity processInstanceEntity) {

        return findByProcessBusinessKeyAndRetryCount(processInstanceEntity.getProcessBusinessKey(), processInstanceEntity.getRetryCount());
    }

    default Optional<ProcessInstanceEntity> findByProcessBusinessKeyAndRetryCount(String instanceBusinessKey, int retryCount) {

        var entries = findAllByProcessBusinessKeyAndRetryCount(instanceBusinessKey, retryCount);

        return Optional.ofNullable(CollectionUtil.last(entries));
    }

    List<ProcessInstanceEntity> findAllByProcessBusinessKeyAndRetryCount(String processBusinessKey, int retryCount);

    default ProcessDefinitionStatus getProcessDefinitionStatus(String definitionKey, TimeRangeType timeRange, LocalDateTime startDate, LocalDateTime endDate) {

        var startDateTime = timeRange.getStartDateTime(startDate);
        var endDateTime = timeRange.getEndDateTime(endDate);

        var definitionStatus = new ProcessDefinitionStatus();

        var processStatus = getProcessStatus(definitionKey, startDateTime, endDateTime);

        definitionStatus.setQueuedCount(getTotalCount(processStatus, ProcessStatus.QUEUED));
        definitionStatus.setProgressCount(getTotalCount(processStatus, ProcessStatus.STARTED));
        definitionStatus.setRestartedCount(getTotalCount(processStatus, ProcessStatus.RESTARTED));
        definitionStatus.setCompletedCount(getTotalCount(processStatus, ProcessStatus.PROCESS_COMPLETED));
        definitionStatus.setTerminatedCount(getTotalCount(processStatus, ProcessStatus.TERMINATED));
        definitionStatus.setWarningCount(getTotalCount(processStatus, ProcessStatus.WARNING));
        definitionStatus.setFailedCount(getTotalCount(processStatus, ProcessStatus.ERROR));

        return definitionStatus;
    }

    @Query(value = "SELECT INSTANCE_STATUS as processStatus, count(*) as totalCount " +
            "FROM PROCESS_INSTANCE_STATUS_VIEW " +
            "WHERE DEFINITION_KEY = ?1 " +
            "AND INSTANCE_START_TIME >= ?2 " +
            "AND INSTANCE_START_TIME <= ?3 " +
            "GROUP BY INSTANCE_STATUS",
            nativeQuery = true)
    List<ProcessStatusProjection> getProcessStatus(String definitionKey, LocalDateTime startDateTime, LocalDateTime endDateTime);

    @Query(value = "SELECT INSTANCE_STATUS AS processStatus, count(*) AS totalCount FROM PROCESS_INSTANCE_VIEW GROUP BY INSTANCE_STATUS", nativeQuery = true)
    List<ProcessStatusProjection> getProcessStatus();

    private long getTotalCount(List<ProcessStatusProjection> list, ProcessStatus processStatus) {

        return list.stream().filter(it -> ProcessStatus.fromOrdinal(it.getProcessStatus()) == processStatus).map(ProcessStatusProjection::getTotalCount).findFirst().orElse(0);
    }

    default Page<ProcessInstanceEntity> findAllTerminated(String definitionKey) {

        var processStatus = ProcessStatus.TERMINATED;
        var beforeTime = LocalDateTime.now().minusHours(1);
        var pageRequest = PageRequest.of(0, 100, Sort.Direction.DESC, "instanceStartTime");

        Specification<ProcessInstanceEntity> specification = (root, criteriaQuery, criteriaBuilder) -> {

            var definitionKeyPredicate = criteriaBuilder.equal(criteriaBuilder.lower(root.get("definitionKey")), definitionKey.toLowerCase());
            var processStatusPredicate = criteriaBuilder.equal(root.get("instanceStatus"), processStatus);
            var instanceStartTimePredicate = criteriaBuilder.lessThan(root.get("instanceStartTime"), beforeTime);

            return criteriaBuilder.and(definitionKeyPredicate, processStatusPredicate, instanceStartTimePredicate);
        };

        return findAll(specification, pageRequest);
    }
}
