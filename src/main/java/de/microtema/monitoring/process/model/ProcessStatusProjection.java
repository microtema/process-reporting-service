package de.microtema.monitoring.process.model;

public interface ProcessStatusProjection {

    int getProcessStatus();

    int getTotalCount();
}
