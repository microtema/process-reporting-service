package de.microtema.monitoring.timerange.controller;

import de.microtema.monitoring.timerange.facade.TimeRangeFacade;
import de.microtema.monitoring.timerange.model.TimeRangeEntry;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/rest/api/time-range")
public class TimeRangeController {

    private final TimeRangeFacade facade;

    @GetMapping
    public ResponseEntity<List<TimeRangeEntry>> getTimeRangeList() {

        return ResponseEntity.ok(facade.getTimeRangeList());
    }
}
