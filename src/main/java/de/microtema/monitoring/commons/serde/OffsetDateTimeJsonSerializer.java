package de.microtema.monitoring.commons.serde;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

public class OffsetDateTimeJsonSerializer extends JsonSerializer<OffsetDateTime> {

    @Override
    public void serialize(OffsetDateTime offsetDateTime, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {

        if (Objects.isNull(offsetDateTime)) {
            return;
        }

        var format = DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(offsetDateTime);

        jsonGenerator.writeString(format);
    }
}
