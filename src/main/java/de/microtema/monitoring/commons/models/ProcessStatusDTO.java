package de.microtema.monitoring.commons.models;

import lombok.Data;

@Data
public class ProcessStatusDTO {

    private String displayName;

    private String value;
}
