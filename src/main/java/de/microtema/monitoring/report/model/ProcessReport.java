package de.microtema.monitoring.report.model;

import de.microtema.monitoring.commons.models.ProcessStatus;

import java.time.LocalDateTime;

public class ProcessReport {

    private String id;

    private String displayName;

    private String processName;

    private String definitionKey;

    private String activityId;

    private String description;

    private ProcessStatus processStatus;

    private String processBusinessKey;

    private String multipleInstanceIndex;

    private String errorMessage;

    private String inputPayload;

    private String outputPayload;

    private LocalDateTime reportStartTime;

    private LocalDateTime reportEndTime;

    private Long duration;

    private int retryCount;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getProcessName() {
        return processName;
    }

    public void setProcessName(String processName) {
        this.processName = processName;
    }

    public String getDefinitionKey() {
        return definitionKey;
    }

    public void setDefinitionKey(String definitionKey) {
        this.definitionKey = definitionKey;
    }

    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ProcessStatus getProcessStatus() {
        return processStatus;
    }

    public void setProcessStatus(ProcessStatus processStatus) {
        this.processStatus = processStatus;
    }

    public String getProcessBusinessKey() {
        return processBusinessKey;
    }

    public void setProcessBusinessKey(String processBusinessKey) {
        this.processBusinessKey = processBusinessKey;
    }

    public String getMultipleInstanceIndex() {
        return multipleInstanceIndex;
    }

    public void setMultipleInstanceIndex(String multipleInstanceIndex) {
        this.multipleInstanceIndex = multipleInstanceIndex;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getInputPayload() {
        return inputPayload;
    }

    public void setInputPayload(String inputPayload) {
        this.inputPayload = inputPayload;
    }

    public String getOutputPayload() {
        return outputPayload;
    }

    public void setOutputPayload(String outputPayload) {
        this.outputPayload = outputPayload;
    }

    public LocalDateTime getReportStartTime() {
        return reportStartTime;
    }

    public void setReportStartTime(LocalDateTime reportStartTime) {
        this.reportStartTime = reportStartTime;
    }

    public LocalDateTime getReportEndTime() {
        return reportEndTime;
    }

    public void setReportEndTime(LocalDateTime reportEndTime) {
        this.reportEndTime = reportEndTime;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public int getRetryCount() {
        return retryCount;
    }

    public void setRetryCount(int retryCount) {
        this.retryCount = retryCount;
    }
}
