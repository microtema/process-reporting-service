package de.microtema.monitoring.report.repository;

import de.microtema.monitoring.commons.models.ProcessStatus;
import de.microtema.monitoring.commons.repository.BaseEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.Immutable;

import jakarta.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@EqualsAndHashCode(callSuper = false)
@Table(name = "PROCESS_REPORT_VIEW")
public class ProcessReportEntity extends BaseEntity {

    @Column(name = "PROCESS_BUSINESS_KEY")
    private String processBusinessKey;

    @Column(name = "INSTANCE_ID")
    private String instanceId;

    @Column(name = "INSTANCE_NAME")
    private String instanceName;

    @Column(name = "DEFINITION_KEY")
    private String definitionKey;

    @Column(name = "REPORT_ID")
    private String reportId;

    @Column(name = "MULTIPLE_INSTANCE_INDEX")
    private String multipleInstanceIndex;

    @Column(name = "REPORT_NAME")
    private String reportName;

    @Column(name = "REPORT_STATUS")
    @Enumerated(value = EnumType.ORDINAL)
    private ProcessStatus reportStatus;

    @Column(name = "REPORT_DESCRIPTION")
    private String reportDescription;

    @Column(name = "REPORT_START_TIME")
    private LocalDateTime reportStartTime;

    @Column(name = "REPORT_END_TIME")
    private LocalDateTime reportEndTime;

    @Column(columnDefinition = "Text", name = "INPUT_PAYLOAD")
    private String inputPayload;

    @Column(columnDefinition = "Text", name = "OUTPUT_PAYLOAD")
    private String outputPayload;

    @Column(columnDefinition = "Text", name = "ERROR_MESSAGE")
    private String errorMessage;

    @Column(name = "RETRY_COUNT")
    private int retryCount;
}
