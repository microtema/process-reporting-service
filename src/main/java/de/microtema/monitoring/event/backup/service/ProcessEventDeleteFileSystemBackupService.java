package de.microtema.monitoring.event.backup.service;

import de.microtema.monitoring.commons.backup.model.BackupType;
import de.microtema.monitoring.commons.backup.service.FileSystemService;
import de.microtema.monitoring.commons.backup.service.ProcessDeleteFileSystemBackupService;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class ProcessEventDeleteFileSystemBackupService extends ProcessDeleteFileSystemBackupService {

    public ProcessEventDeleteFileSystemBackupService(FileSystemService fileSystemService,
                                                     ProcessEventReadFileSystemBackupService readFileSystemBackupService) {
        super(fileSystemService, readFileSystemBackupService);
    }

    @Override
    public BackupType getBackupType() {
        return BackupType.EVENT;
    }
}
