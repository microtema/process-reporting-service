package de.microtema.monitoring.process.model;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

public class ProcessInstancePage extends PageImpl<ProcessInstance> {

    public ProcessInstancePage(Page<ProcessInstance> page) {
        super(page.getContent(), page.getPageable(), page.getTotalElements());
    }
}
