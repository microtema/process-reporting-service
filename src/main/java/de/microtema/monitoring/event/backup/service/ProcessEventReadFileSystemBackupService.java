package de.microtema.monitoring.event.backup.service;

import de.microtema.monitoring.commons.backup.model.BackupType;
import de.microtema.monitoring.commons.backup.service.FileSystemService;
import de.microtema.monitoring.commons.backup.service.ProcessReadFileSystemBackupService;
import de.microtema.monitoring.definition.service.ProcessDefinitionService;
import de.microtema.monitoring.event.backup.converter.StringToProcessFileSystemConverter;
import org.springframework.stereotype.Service;

@Service
public class ProcessEventReadFileSystemBackupService extends ProcessReadFileSystemBackupService {

    public ProcessEventReadFileSystemBackupService(FileSystemService fileSystemService, ProcessDefinitionService processDefinitionService,
                                                   StringToProcessFileSystemConverter processInstanceFileSystemConverter) {
        super(fileSystemService, processDefinitionService, processInstanceFileSystemConverter);
    }

    @Override
    public BackupType getBackupType() {
        return BackupType.EVENT;
    }
}
