package de.microtema.monitoring.commons.backup.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.microtema.monitoring.commons.backup.model.BackupType;
import de.microtema.monitoring.commons.repository.BaseEntity;
import de.microtema.monitoring.commons.repository.BaseEntityRepository;
import jakarta.persistence.criteria.Path;
import jakarta.persistence.criteria.Predicate;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.List;
import java.util.function.BiFunction;

@Log4j2
@RequiredArgsConstructor
public abstract class ProcessWriteFileSystemBackupService<T extends BaseEntity> {

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy_MM_dd_HH_mm");

    private final BiFunction<String, LocalDateTime, Specification<T>> specificationFunction = (definitionKey, retentionTime) -> (root, criteriaQuery, criteriaBuilder) -> {

        Path<String> path = root.get("definitionKey");

        Predicate definitionKeyPredicate = criteriaBuilder.equal(path, definitionKey);

        Path<LocalDateTime> startTimePath = root.get(getStartTimeProperty());

        var instanceStartTimePredicate = criteriaBuilder.lessThan(startTimePath, retentionTime);

        return criteriaBuilder.and(definitionKeyPredicate, instanceStartTimePredicate);
    };

    private final FileSystemService fileSystemService;

    private final ObjectMapper objectMapper;

    private final BaseEntityRepository<T> repository;

    @Value("${reporting.file-system.backup-batch-size:1000}")
    private int batchSize = 1000;

    public abstract BackupType getBackupType();

    public abstract String getStartTimeProperty();

    @SneakyThrows
    public void backup(String definitionKey, LocalDateTime retentionDateTime) {

        backupPage(definitionKey, retentionDateTime, 0);
    }

    @SneakyThrows
    public void backup(String definitionKey, int pageIndex, Page<T> page) {

        if (!fileSystemService.existsBackupFolder()) {
            return;
        }

        if (page.isEmpty()) {
            return;
        }

        var content = page.getContent();

        var startDateTime = getStartTime(content.get(0));

        var backupFile = createBackupFile(definitionKey, pageIndex, page.getNumberOfElements(), startDateTime);

        var fileOutputStream = new FileOutputStream(backupFile);

        objectMapper.writeValue(fileOutputStream, content);
    }

    @SneakyThrows
    private LocalDateTime getStartTime(T entity) {

        return (LocalDateTime) PropertyUtils.getProperty(entity, getStartTimeProperty());
    }

    @SneakyThrows
    private void backupPage(String definitionKey, LocalDateTime retentionDateTime, int pageIndex) {

        var page = getQueryPage(definitionKey, retentionDateTime);

        if (page.isEmpty()) {
            return;
        }

        var backupType = getBackupType().name().toLowerCase();

        var content = page.getContent();
        var totalElements = page.getTotalElements();

        backup(definitionKey, pageIndex, page);

        repository.deleteAllInBatch(content);
        repository.flush();

        log.info("{}/{} {} {}(s) older then [{}] removed from database and stored as backup", content.size(), totalElements, definitionKey, backupType, retentionDateTime);

        backupPage(definitionKey, retentionDateTime, pageIndex + 1);
    }

    private Page<T> getQueryPage(String definitionKey, LocalDateTime localDateTime) {

        var pageRequest = PageRequest.of(0, batchSize, Sort.Direction.DESC, getStartTimeProperty());

        return repository.findAll(specificationFunction.apply(definitionKey, localDateTime), pageRequest);
    }

    private File createBackupFile(String definitionKey, int batchIndex, int numberOfElements, LocalDateTime startDateTime) {

        var backupType = getBackupType();

        var backupFolderFile = new File(fileSystemService.getBackupFolder(), backupType.name());

        var timestamp = DATE_FORMAT.format(Timestamp.valueOf(startDateTime));

        var fileName = timestamp + "__" + batchIndex + "_" + numberOfElements + ".json";

        var dir = new File(backupFolderFile, definitionKey);

        if (!dir.exists()) {
            Validate.isTrue(dir.mkdirs(), "Unable to create dir: " + dir.getPath());
        }

        return new File(dir, fileName);
    }
}
