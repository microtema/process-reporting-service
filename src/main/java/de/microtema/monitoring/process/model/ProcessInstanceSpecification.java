package de.microtema.monitoring.process.model;

import de.microtema.monitoring.commons.models.ProcessStatus;
import de.microtema.monitoring.timerange.model.TimeRangeType;
import lombok.Builder;
import lombok.Value;

import java.time.LocalDateTime;

@Builder
@Value
public class ProcessInstanceSpecification {

    String query;

    String definitionKey;

    ProcessStatus processStatus;

    TimeRangeType timeRange;

    LocalDateTime startDate;

    LocalDateTime endDate;
}
