package de.microtema.monitoring.timerange.model;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoField;
import java.util.Objects;
import java.util.Optional;


public enum TimeRangeType {

    TODAY("Today") {
        @Override
        public LocalDateTime getStartDateTime() {
            return LocalDateTime.now().toLocalDate().atStartOfDay();
        }

        @Override
        public LocalDateTime getEndDateTime() {
            return LocalDateTime.now().toLocalDate().atTime(LocalTime.MAX);
        }
    },
    LAST_DAY("Last Day") {
        @Override
        public LocalDateTime getStartDateTime() {

            return LocalDateTime.now().minusDays(1).toLocalDate().atStartOfDay();
        }

        @Override
        public LocalDateTime getEndDateTime() {
            return LocalDateTime.now().minusDays(1).toLocalDate().atTime(LocalTime.MAX);
        }
    },
    THIS_WEEK("This Week") {
        @Override
        public LocalDateTime getStartDateTime() {
            return LocalDateTime.now().with(DayOfWeek.MONDAY).toLocalDate().atStartOfDay();
        }

        @Override
        public LocalDateTime getEndDateTime() {
            return LocalDateTime.now().toLocalDate().atTime(LocalTime.MAX);
        }
    },
    LAST_WEEK("Last Week") {
        @Override
        public LocalDateTime getStartDateTime() {
            return LocalDateTime.now().with(DayOfWeek.MONDAY).toLocalDate().minusWeeks(1).atStartOfDay();
        }

        @Override
        public LocalDateTime getEndDateTime() {
            return LocalDateTime.now().with(DayOfWeek.SUNDAY).toLocalDate().minusWeeks(1).atTime(LocalTime.MAX);
        }
    },
    THIS_MONTH("This Month") {
        @Override
        public LocalDateTime getStartDateTime() {
            return LocalDateTime.now().toLocalDate().with(ChronoField.DAY_OF_MONTH, 1).atStartOfDay();
        }

        @Override
        public LocalDateTime getEndDateTime() {
            return LocalDateTime.now().toLocalDate().atTime(LocalTime.MAX);
        }
    },
    LAST_MONTH("Last Month") {
        @Override
        public LocalDateTime getStartDateTime() {
            return LocalDateTime.now().with(ChronoField.DAY_OF_MONTH, 1).toLocalDate().minusMonths(1).atStartOfDay();
        }

        @Override
        public LocalDateTime getEndDateTime() {
            return LocalDateTime.now().toLocalDate().minusMonths(1).atTime(LocalTime.MAX);
        }
    },
    ALL("All Time Ranges") {
        @Override
        public LocalDateTime getStartDateTime() {
            return LocalDateTime.now().with(ChronoField.DAY_OF_MONTH, 1).toLocalDate().minusYears(100).atStartOfDay();
        }

        @Override
        public LocalDateTime getEndDateTime() {
            return LocalDateTime.now().toLocalDate().atTime(LocalTime.MAX);
        }
    },
    CUSTOM("Custom Time Range") {
        @Override
        public LocalDateTime getStartDateTime() {
            return null;
        }

        @Override
        public LocalDateTime getEndDateTime() {
            return null;
        }
    };

    private final String displayName;

    TimeRangeType(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public abstract LocalDateTime getStartDateTime();

    public abstract LocalDateTime getEndDateTime();

    public  LocalDateTime getStartDateTime(LocalDateTime startDate) {
        if(this == TimeRangeType.CUSTOM) {
            return Optional.ofNullable(startDate).orElseGet(TimeRangeType.ALL::getStartDateTime);
        }

        return getStartDateTime();
    }

    public  LocalDateTime getEndDateTime(LocalDateTime endDate) {

        if(this == TimeRangeType.CUSTOM) {
            return Optional.ofNullable(endDate).orElseGet(TimeRangeType.ALL::getEndDateTime);
        }

        return getEndDateTime();
    }

    public boolean between(LocalDateTime creationTime, LocalDateTime startDate, LocalDateTime endDate) {

        if (Objects.isNull(creationTime)) {
            return true;
        }

        LocalDateTime startDateTime = getStartDateTime(startDate);

        if (Objects.isNull(startDateTime)) {
            return true;
        }

        if (creationTime.isBefore(startDateTime)) {
            return false;
        }

        LocalDateTime endDateTime = getEndDateTime(endDate);

        if (Objects.isNull(endDateTime)) {
            return true;
        }

        return !creationTime.isAfter(endDateTime);
    }
}
