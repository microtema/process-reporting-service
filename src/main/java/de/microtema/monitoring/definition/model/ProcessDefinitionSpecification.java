package de.microtema.monitoring.definition.model;

import de.microtema.monitoring.timerange.model.TimeRangeType;
import lombok.Builder;
import lombok.Value;

import java.time.LocalDateTime;

@Builder
@Value
public class ProcessDefinitionSpecification {

    String query;

    String groupBy;

    TimeRangeType timeRange;

    LocalDateTime startDate;

    LocalDateTime endDate;
}
