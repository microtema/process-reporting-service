package de.microtema.monitoring.process.converter;

import de.microtema.monitoring.csv.converter.AbstractCsvPageMessageConverter;
import de.microtema.monitoring.process.model.ProcessInstance;
import de.microtema.monitoring.process.model.ProcessInstancePage;
import org.springframework.stereotype.Component;

@Component
public class ProcessInstanceCsvPageMessageConverter extends AbstractCsvPageMessageConverter<ProcessInstancePage, ProcessInstance> {

}
