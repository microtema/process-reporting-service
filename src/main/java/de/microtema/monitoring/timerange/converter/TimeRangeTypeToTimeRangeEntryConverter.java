package de.microtema.monitoring.timerange.converter;

import de.microtema.model.converter.Converter;
import de.microtema.monitoring.timerange.model.TimeRangeEntry;
import de.microtema.monitoring.timerange.model.TimeRangeType;
import org.springframework.stereotype.Component;

@Component
public class TimeRangeTypeToTimeRangeEntryConverter implements Converter<TimeRangeEntry, TimeRangeType> {

    @Override
    public void update(TimeRangeEntry dest, TimeRangeType orig) {

        dest.setValue(orig.name());
        dest.setDisplayName(orig.getDisplayName());
    }
}
