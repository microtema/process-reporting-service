package de.microtema.monitoring.log.service;


import de.microtema.monitoring.commons.converter.PageRequestDTOToPageRequestConverter;
import de.microtema.monitoring.commons.models.PageRequestDTO;
import de.microtema.monitoring.log.converter.LogEntityToLogDataConverter;
import de.microtema.monitoring.log.converter.LogLevelToLogLevelDTOConverter;
import de.microtema.monitoring.log.converter.LogSpecificationToSpecificationConverter;
import de.microtema.monitoring.log.converter.RuntimeNameToRuntimeDataConverter;
import de.microtema.monitoring.log.model.*;
import de.microtema.monitoring.log.repository.LogRepository;
import de.microtema.monitoring.timerange.model.TimeRangeType;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class LogReadService {

    private final LogRepository repository;
    private final LogEntityToLogDataConverter logEntityToLogDataConverter;
    private final PageRequestDTOToPageRequestConverter pageRequestConverter;
    private final LogSpecificationToSpecificationConverter specificationConverter;
    private final LogLevelToLogLevelDTOConverter logLevelToLogLevelDTOConverter;
    private final RuntimeNameToRuntimeDataConverter runtimeNameToRuntimeDataConverter;

    public LogData getLogData(String logId) {

        return repository.findById(logId).map(logEntityToLogDataConverter::convert).orElse(null);
    }

    public Page<LogData> getLogDataPage(int page, int size, String properties, String query, String runtimeName, LogLevel logLevel, TimeRangeType timeRange, LocalDateTime startDate, LocalDateTime endDate) {

        var pageRequest = PageRequestDTO.of(page, size, properties);
        var pageable = pageRequestConverter.convert(pageRequest);
        var logSpecification = LogSpecification.builder()
                .timeRange(timeRange)
                .logLevel(logLevel)
                .runtimeName(runtimeName)
                .query(query)
                .startDate(startDate)
                .endDate(endDate)
                .build();
        var specification = specificationConverter.convert(logSpecification);

        return repository.findAll(specification, pageable).map(logEntityToLogDataConverter::convert);
    }

    public List<LogLevelDTO> getLogLevelList() {

        return logLevelToLogLevelDTOConverter.convertList(LogLevel.values());
    }

    public List<RuntimeData> getRunTimes() {

        var runtimeNames = repository.findAllRuntimeNames();

        return runtimeNameToRuntimeDataConverter.convertList(runtimeNames);
    }
}
