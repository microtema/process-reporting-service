package de.microtema.monitoring.sql.model;

import lombok.Data;

@Data
public class SqlRequest {

    private String sql;
}
