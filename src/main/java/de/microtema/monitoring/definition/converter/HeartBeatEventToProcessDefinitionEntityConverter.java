package de.microtema.monitoring.definition.converter;

import de.microtema.model.converter.Converter;
import de.microtema.monitoring.definition.repository.ProcessDefinitionEntity;
import de.microtema.monitoring.event.command.model.HeartBeatEvent;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Optional;

@Component
public class HeartBeatEventToProcessDefinitionEntityConverter implements Converter<ProcessDefinitionEntity, HeartBeatEvent> {

    @Override
    public void update(ProcessDefinitionEntity dest, HeartBeatEvent orig) {

        var processVersion = Optional.ofNullable(orig.getProcessVersion()).orElse("1.0");
        var versionParts = processVersion.split("\\.");
        var majorVersion = Integer.parseInt(versionParts[0]);
        var minorVersion = Integer.parseInt(versionParts[1]);

        dest.setDefinitionMajorVersion(majorVersion);
        dest.setDefinitionVersion(minorVersion);

        dest.setDefinitionKey(orig.getProcessId());

        dest.setDefinitionDeployTime(LocalDateTime.now());
        dest.setLastHeartBeat(orig.getEventTime());
    }
}
