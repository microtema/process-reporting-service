package de.microtema.monitoring.log.converter;

import de.microtema.model.converter.Converter;
import de.microtema.monitoring.log.model.RuntimeData;
import org.springframework.stereotype.Component;

@Component
public class RuntimeNameToRuntimeDataConverter implements Converter<RuntimeData, String> {

    @Override
    public void update(RuntimeData dest, String orig) {

        dest.setDisplayName(orig);
        dest.setDefinitionKey(orig);
    }
}
