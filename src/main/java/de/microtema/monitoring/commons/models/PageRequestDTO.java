package de.microtema.monitoring.commons.models;

import lombok.Value;

@Value(staticConstructor = "of")
public class PageRequestDTO {

    int page;

    int size;

    String properties;
}
