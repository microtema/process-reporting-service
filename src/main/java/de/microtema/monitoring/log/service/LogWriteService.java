package de.microtema.monitoring.log.service;

import de.microtema.monitoring.log.converter.LogEventToLogEntityConverter;
import de.microtema.monitoring.log.model.LogReportEvent;
import de.microtema.monitoring.log.repository.LogRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Log4j2
@Service
@Transactional
@RequiredArgsConstructor
public class LogWriteService {

    private final LogRepository repository;

    private final LogEventToLogEntityConverter logEventToLogEntityConverter;

    @Async
    public String saveLogEvent(LogReportEvent logEvent) {

        var logEntity = logEventToLogEntityConverter.convert(logEvent);

        return repository.save(logEntity).getUuid();
    }
}
