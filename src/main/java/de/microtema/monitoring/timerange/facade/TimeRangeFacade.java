package de.microtema.monitoring.timerange.facade;

import de.microtema.monitoring.timerange.model.TimeRangeEntry;
import de.microtema.monitoring.timerange.service.TimeRangeService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class TimeRangeFacade {

    private final TimeRangeService service;

    public List<TimeRangeEntry> getTimeRangeList() {

        return service.getTimeRangeList();
    }
}
