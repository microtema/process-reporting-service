package de.microtema.monitoring.log.repository;

import de.microtema.monitoring.commons.repository.BaseEntity;
import de.microtema.monitoring.log.model.LogLevel;
import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "LOG_DATA")
@EqualsAndHashCode(callSuper = false)
public class LogEntity extends BaseEntity {

    @Column(name = "RUNTIME_NAME")
    private String runtimeName;

    @Column(name = "RUNTIME_ID")
    private String runtimeId;

    @Column(columnDefinition = "Text", name = "LOG_MESSAGE")
    private String logMessage;

    @Column(columnDefinition = "Text", name = "STACK_TRACE")
    private String stackTrace;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "LOG_LEVEL")
    private LogLevel logLevel;

    @Column(name = "LOG_TIMESTAMP")
    private LocalDateTime logTimestamp;
}
