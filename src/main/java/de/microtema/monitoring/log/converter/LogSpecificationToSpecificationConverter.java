package de.microtema.monitoring.log.converter;

import de.microtema.model.converter.Converter;
import de.microtema.monitoring.log.model.LogLevel;
import de.microtema.monitoring.log.model.LogSpecification;
import de.microtema.monitoring.log.repository.LogEntity;
import de.microtema.monitoring.timerange.model.TimeRangeType;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.Path;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Objects;

@Component
@RequiredArgsConstructor
public class LogSpecificationToSpecificationConverter implements Converter<Specification<LogEntity>, LogSpecification> {

    @Override
    public Specification<LogEntity> convert(LogSpecification orig) {

        return (root, criteriaQuery, criteriaBuilder) -> {

            var predicate = getEqualsPredicate(root, criteriaBuilder, "runtimeName", orig.getRuntimeName());
            var logLevelPredicate = getLogLevelPredicate(root, criteriaBuilder, orig.getLogLevel());
            var timeRangePredicate = getTimeRangePredicate(root, criteriaBuilder, orig.getTimeRange(), orig.getStartDate(), orig.getEndDate());

            predicate = criteriaBuilder.and(predicate, logLevelPredicate);

            if (Objects.nonNull(timeRangePredicate)) {

                predicate = criteriaBuilder.and(predicate, timeRangePredicate);
            }

            var orPredicate = getOrPredicate(orig, root, criteriaBuilder);

            if (Objects.nonNull(orPredicate)) {

                predicate = criteriaBuilder.and(predicate, orPredicate);
            }

            return criteriaBuilder.and(predicate);
        };
    }

    private Predicate getEqualsPredicate(Root<LogEntity> root, CriteriaBuilder criteriaBuilder, String property, String query) {

        if (StringUtils.isEmpty(query)) {
            return criteriaBuilder.and();
        }

        Path<String> path = root.get(property);

        return criteriaBuilder.equal(criteriaBuilder.lower(path), query.toLowerCase());
    }

    private Predicate getTimeRangePredicate(Root<LogEntity> root, CriteriaBuilder criteriaBuilder, TimeRangeType timeRange, LocalDateTime startDate, LocalDateTime endDate) {

        var startDateTime = timeRange.getStartDateTime(startDate);
        var endDateTime = timeRange.getEndDateTime(endDate);

        return criteriaBuilder.between(root.get("logTimestamp"), startDateTime, endDateTime);
    }

    private Predicate getOrPredicate(LogSpecification orig, Root<LogEntity> root, CriteriaBuilder criteriaBuilder) {

        var predicates = new ArrayList<Predicate>();

        var predicate = getLikePredicate(root, criteriaBuilder, "logMessage", StringUtils.trimToEmpty(orig.getQuery()));
        CollectionUtils.addIgnoreNull(predicates, predicate);

        predicate = getLikePredicate(root, criteriaBuilder, "runtimeId", StringUtils.trimToEmpty(orig.getQuery()));
        CollectionUtils.addIgnoreNull(predicates, predicate);

        if (CollectionUtils.isEmpty(predicates)) {
            return null;
        }

        return criteriaBuilder.or(predicates.toArray(new Predicate[0]));
    }

    private Predicate getLogLevelPredicate(Root<LogEntity> root, CriteriaBuilder criteriaBuilder, LogLevel logLevel) {

        if (LogLevel.ALL == logLevel || Objects.isNull(logLevel)) {
            return criteriaBuilder.and();
        }

        Path<LogLevel> path = root.get("logLevel");

        return criteriaBuilder.equal(path, logLevel);
    }

    private Predicate getLikePredicate(Root<LogEntity> root, CriteriaBuilder criteriaBuilder, String property, String query) {

        if (Objects.isNull(query)) {
            return null;
        }

        Path<String> path = root.get(property);

        return criteriaBuilder.like(criteriaBuilder.lower(path), "%" + query.toLowerCase() + "%");
    }
}
